﻿namespace Polar_Cycle_App_Siddhartha
{
    partial class IntervDetection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.lblThres = new MetroFramework.Controls.MetroLabel();
            this.lblAvg = new MetroFramework.Controls.MetroLabel();
            this.grd_interval = new System.Windows.Forms.DataGridView();
            this.clm_Interval = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm_power = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Interval = new MetroFramework.Controls.MetroLabel();
            this.listBox1 = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.grd_interval)).BeginInit();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(94, 266);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(98, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Average Power";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(86, 221);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(106, 19);
            this.metroLabel2.TabIndex = 0;
            this.metroLabel2.Text = "Threshold Power";
            // 
            // lblThres
            // 
            this.lblThres.AutoSize = true;
            this.lblThres.Location = new System.Drawing.Point(210, 221);
            this.lblThres.Name = "lblThres";
            this.lblThres.Size = new System.Drawing.Size(33, 19);
            this.lblThres.TabIndex = 1;
            this.lblThres.Text = "0.00";
            // 
            // lblAvg
            // 
            this.lblAvg.AutoSize = true;
            this.lblAvg.Location = new System.Drawing.Point(210, 266);
            this.lblAvg.Name = "lblAvg";
            this.lblAvg.Size = new System.Drawing.Size(33, 19);
            this.lblAvg.TabIndex = 1;
            this.lblAvg.Text = "0.00";
            // 
            // grd_interval
            // 
            this.grd_interval.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_interval.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clm_Interval,
            this.clm_power});
            this.grd_interval.Location = new System.Drawing.Point(568, 73);
            this.grd_interval.Name = "grd_interval";
            this.grd_interval.Size = new System.Drawing.Size(243, 350);
            this.grd_interval.TabIndex = 2;
            // 
            // clm_Interval
            // 
            this.clm_Interval.HeaderText = "Interval";
            this.clm_Interval.Name = "clm_Interval";
            // 
            // clm_power
            // 
            this.clm_power.HeaderText = "Power";
            this.clm_power.Name = "clm_power";
            // 
            // Interval
            // 
            this.Interval.AutoSize = true;
            this.Interval.Location = new System.Drawing.Point(664, 51);
            this.Interval.Name = "Interval";
            this.Interval.Size = new System.Drawing.Size(52, 19);
            this.Interval.TabIndex = 3;
            this.Interval.Text = "Interval";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(364, 74);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(179, 342);
            this.listBox1.TabIndex = 4;
            this.listBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseClick);
            // 
            // IntervDetection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 446);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.Interval);
            this.Controls.Add(this.grd_interval);
            this.Controls.Add(this.lblAvg);
            this.Controls.Add(this.lblThres);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Name = "IntervDetection";
            this.Text = "Interval Detection";
            this.Load += new System.EventHandler(this.IntervDetection_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grd_interval)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel lblThres;
        private MetroFramework.Controls.MetroLabel lblAvg;
        private System.Windows.Forms.DataGridView grd_interval;
        private MetroFramework.Controls.MetroLabel Interval;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_Interval;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_power;
        private System.Windows.Forms.ListBox listBox1;
    }
}