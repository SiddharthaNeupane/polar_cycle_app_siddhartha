﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace Polar_Cycle_App_Siddhartha
{
    public partial class graph : MetroFramework.Forms.MetroForm
    {
        main_class m = new main_class();
        public graph()
        {
            InitializeComponent();
        }

        PointPairList list1;
        PointPairList list2;
        PointPairList list3;
        PointPairList list4;
        PointPairList list5;

        LineItem teamACurve;
        LineItem teamBCurve;
        LineItem teamCCurve;
        LineItem teamDCurve;
        LineItem teamECurve;
        delegate void SetTextCallback(string text);
        delegate void axisChangeZedGraphCallBack(ZedGraphControl zg);
        public Thread garthererThread;

        private void zedGraph()
        {
            
            try
            {
                // start
               
                double[] heartRate = OpenFile.graphHeartRate;
                double[] speed = OpenFile.graphSpeed;
                double[] cadence = OpenFile.graphCadence;
                double[] altitude = OpenFile.graphAltitude;
                double[] power = OpenFile.graphPower;

                GraphPane graphValue = zedGraphControl1.GraphPane;
                // Set the Titles
                graphValue.Title = "Polar Cycle Computer";
                //graphValue.XAxis.Title = "My X Axis";
               // graphValue.YAxis.Title = "My Y Axis";
               // graphValue.Title.FontSpec.FontColor = Color.Crimson;

                // Add gridlines to the plot, and make them gray
                double x, y1, y2, y3, y4, y5;

                // Move the legend location
                graphValue.Legend.Position = ZedGraph.LegendPos.Top;
                PointPairList teamAPairList = new PointPairList();
                PointPairList teamBPairList = new PointPairList();
                PointPairList teamCPairList = new PointPairList();
                PointPairList teamDPairList = new PointPairList();
                PointPairList teamEPairList = new PointPairList();


                graphValue.XAxis.Min = 1;
                graphValue.XAxis.Max = 5000;

                graphValue.YAxis.Min = 1;
                graphValue.YAxis.Max = 600;

                for (int i = 0; i < heartRate.Length; i++)
                {
                    teamAPairList.Add(i, heartRate[i]);

                }
                for (int i = 0; i < speed.Length; i++)
                {

                    teamBPairList.Add(i, speed[i]);
                }
                for (int i = 0; i < cadence.Length; i++)
                {

                    teamCPairList.Add(i, cadence[i]);
                }
                for (int i = 0; i < power.Length; i++)
                {

                    teamDPairList.Add(i, power[i]);
                }
                for (int i = 0; i < altitude.Length; i++)
                {

                    teamEPairList.Add(i, altitude[i]);
                }

                teamACurve = graphValue.AddCurve("Heart Rate",teamAPairList, Color.Red, SymbolType.None);
                teamBCurve = graphValue.AddCurve("Speed ",teamBPairList, Color.Blue, SymbolType.None);
                teamDCurve = graphValue.AddCurve("Cadence",teamDPairList, Color.Yellow, SymbolType.None);
                teamECurve = graphValue.AddCurve("Altitude ", teamEPairList, Color.Orange, SymbolType.None);
                teamCCurve = graphValue.AddCurve("Power",teamCPairList, Color.Green, SymbolType.None);

                //  zedGraphControl1.IsEnableHZoom = true;
                //  zedGraphControl1.IsEnableVZoom = false;

                axisChangeZedGraph(zedGraphControl1);

                SetSize();

                // end
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void axisChangeZedGraph(ZedGraphControl zg)
        {
            try
            {
                if (zg.InvokeRequired)
                {
                    axisChangeZedGraphCallBack ad = new axisChangeZedGraphCallBack(axisChangeZedGraph);
                    zg.Invoke(ad, new object[] { zg });
                }
                else
                {
                    zedGraphControl1.AxisChange();
                    zg.Invalidate();
                    zg.Refresh();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
        private void SetSize()
        {
            try
            {
                zedGraphControl1.Location = new Point(0, 0);
                zedGraphControl1.IsShowPointValues = true;
                // Leave a small margin around the outside of the control
                zedGraphControl1.Size = new Size(this.ClientRectangle.Width - 50, this.ClientRectangle.Height - 50);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }

        private void chkHeartRate_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkHeartRate.Checked == false)
                {
                    zedGraphControl1.GraphPane.CurveList.Remove(teamACurve);
                }
                else
                {
                    zedGraphControl1.GraphPane.CurveList.Add(teamACurve);
                }
                zedGraphControl1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void chkSpeed_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkSpeed.Checked == false)
                {
                    zedGraphControl1.GraphPane.CurveList.Remove(teamBCurve);
                }
                else
                {
                    zedGraphControl1.GraphPane.CurveList.Add(teamBCurve);
                }
                zedGraphControl1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void chkCadence_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkCadence.Checked == false)
                {
                    zedGraphControl1.GraphPane.CurveList.Remove(teamDCurve);
                }
                else
                {
                    zedGraphControl1.GraphPane.CurveList.Add(teamDCurve);
                }
                zedGraphControl1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void chkAltitude_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkAltitude.Checked == false)
                {
                    zedGraphControl1.GraphPane.CurveList.Remove(teamECurve);
                }
                else
                {
                    zedGraphControl1.GraphPane.CurveList.Add(teamECurve);
                }
                zedGraphControl1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void chkPower_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkPower.Checked == false)
                {
                    zedGraphControl1.GraphPane.CurveList.Remove(teamCCurve);
                }
                else
                {
                    zedGraphControl1.GraphPane.CurveList.Add(teamCCurve);
                }
                zedGraphControl1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void graph_Load(object sender, EventArgs e)
        {
            zedGraph();
            SetSize();
        }

        private void zedGraphControl1_Load(object sender, EventArgs e)
        {
            // zedGraphControl1.EnableHZoom = true;
            // zedGraphControl1.EnableVZoom = false;
            zedGraphControl1.Update();
        }
    }
}
