﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Polar_Cycle_App_Siddhartha
{
    public partial class ChunkData : MetroFramework.Forms.MetroForm
    {
        int ChunkNo;
        public int chunkGet { get; set; }
        string i;

        public ChunkData()
        {
            InitializeComponent();
        }

        private void ChunkData_Load(object sender, EventArgs e)
        {
            OK.DialogResult = DialogResult.OK;
        }

        private void OK_Click(object sender, EventArgs e)
        {

            try
            {
                this.chunkGet = Convert.ToInt32(inputValue.Text);
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
                
           
            
        }

        private void inputValue_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                i = inputValue.SelectedValue.ToString();
                ChunkNo = Convert.ToInt32(i);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
    }
}
