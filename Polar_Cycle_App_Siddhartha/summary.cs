﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Polar_Cycle_App_Siddhartha
{
    public partial class summary : MetroFramework.Forms.MetroForm
    {
        public summary()
        {
            InitializeComponent();
            unit_data_kmPerhr();
        }

        public void unit_data_kmPerhr()
        {
            lblAverageSpeed.Text = System.Math.Round(OpenFile.averageSpeed, 2) + " Km/h";
            lblMaximumSpeed.Text = OpenFile.maxSpeed.ToString() + " Km/h";
            lblAverageHeartRate.Text = System.Math.Round(OpenFile.averageHeartRate, 2) + " bpm";
            lblMaximumHeartRate.Text = OpenFile.maxHeartRate.ToString() + " bpm";
            lblMinimumHeartRate.Text = OpenFile.minHeartRate.ToString() + " bpm";
            lblAveragePower.Text = System.Math.Round(OpenFile.averagePower, 2) + " W";
            lblMaximumPower.Text = OpenFile.maxPower.ToString() + " W";
            lblAverageAltitude.Text = System.Math.Round(OpenFile.averageAltitude, 2) + " m";
            lblMaximumAltitude.Text = OpenFile.maxAltitude.ToString() + " m";
            lblTotalDistance.Text = OpenFile.totalDistance.ToString() + " Km";
        }
        public void unit_data_mile()
        {
            lblAverageSpeed.Text = System.Math.Round(OpenFile.averageSpeedMiles, 2) + " miles";
            lblMaximumSpeed.Text = OpenFile.maxSpeedMiles.ToString() + " miles";
            lblAverageHeartRate.Text = System.Math.Round(OpenFile.averageHeartRate, 2) + " bpm";
            lblMaximumHeartRate.Text = OpenFile.maxHeartRate.ToString() + " bpm";
            lblMinimumHeartRate.Text = OpenFile.minHeartRate.ToString() + " bpm";
            lblAveragePower.Text = System.Math.Round(OpenFile.averagePower, 2) + " W";
            lblMaximumPower.Text = OpenFile.maxPower.ToString() + " W";
            lblAverageAltitude.Text = System.Math.Round(OpenFile.averageAltitudeMile, 2) + " Ft";
            lblMaximumAltitude.Text = System.Math.Round(OpenFile.maxAltitudeMile) + " Ft";
            lblTotalDistance.Text = OpenFile.totalDistanceMiles.ToString() + " miles";
        }

        private void radioKm_CheckedChanged(object sender, EventArgs e)
        {
            unit_data_kmPerhr();
        }
        private void radioMiles_CheckedChanged(object sender, EventArgs e)
        {
            unit_data_mile();
        }
    }
}
