﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Polar_Cycle_App_Siddhartha
{
    public partial class AdvanceMatrixCalc : MetroFramework.Forms.MetroForm
    {
        public AdvanceMatrixCalc()
        {
            InitializeComponent();
        }

        /// <summary>
        /// all the values here are derived from the main dashboard 
        /// that is the openfile form 
        /// the valeus are taken and the lables are replaced with the detrived values 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdvanceMatrixCalc_Load(object sender, EventArgs e)
        {
            try
            {
                lblNorPow.Text = OpenFile.normalizationPowerGlobal.ToString();
                lblMovAvg.Text = OpenFile.avgPowerGlobal.ToString();
                lblFuncThres.Text = OpenFile.ftpGlobal.ToString();
                lblIntesiFac.Text = OpenFile.ifGlobal.ToString();
                lblTraiStres.Text = OpenFile.tssGlobal.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Some error Occured \n" + ex);
            }

        }
    }
}
