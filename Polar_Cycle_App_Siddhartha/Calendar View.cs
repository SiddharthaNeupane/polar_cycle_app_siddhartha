﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Polar_Cycle_App_Siddhartha
{
    public partial class Calendar_View : MetroFramework.Forms.MetroForm
    {
        /// <summary>
        /// the constructor for the calendar view component.
        /// </summary>
        public Calendar_View()
        {
            InitializeComponent();
        }

        /// <summary>
        /// declaration of variables for the calendar data storing 
        /// </summary>
        string storeData;
        string dateStart = "1/23/2018";
        string dateFinal;

        double movAvgCount;
        FolderBrowserDialog fd = new FolderBrowserDialog();
        string[] fdata;
        public static List<List<double>> intervalValues = new List<List<double>>();
        public static List<double> powerData = new List<double>(); // used in interval detection as well 
        public static List<double> intervalDetectionData = new List<double>(); // interval detection 
        public static List<double> powerInterval = new List<double>(); // interval detection 
        public static double threholdValueGlobal;  // interval detection 
        List<double> powerDataSlt = new List<double>();


        public static double ftpGlobal { get; set; }
        public static double ifGlobal { get; set; }
        public static double tssGlobal { get; set; }
        public static double avgPowerGlobal { get; set; }
        public static double normalizationPowerGlobal { get; set; }

        List<double> movAvgPow4 = new List<double>();
        List<double> movAvg = new List<double>();
        List<double> movAvgPow4Slt = new List<double>();
        List<double> movAvgSlt = new List<double>();

        /// <summary>
        /// this chunk of code is called when the open folder button is clicked 
        /// to take input of all the datas of the whole month
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOpenCalendar_Click(object sender, EventArgs e)
        {
            try
            {
                //if the diallog box has been clicked with ok button
                if (fd.ShowDialog() == DialogResult.OK)
                {
                    //get the files from the directory.
                    //all the file names will be selected from the directory and stored in the fdata array 
                    fdata = Directory.GetFiles(fd.SelectedPath);
                    DateTime timeValue;
                    string valueTwo = fdata[0];

                    // start  to highlight the starting month in monthcalender
                    //reading the files to highlight the calendar 
                     StreamReader fileReaderFolderst = new StreamReader(valueTwo);
                    while (!fileReaderFolderst.EndOfStream)
                    {
                        storeData = fileReaderFolderst.ReadLine();
                        if (storeData.Contains("Date"))
                        {
                            string startTime = storeData;
                            string arraStartTime = startTime.Split('=').Last();
                            var dt = DateTime.ParseExact(arraStartTime, "yyyyMMdd", CultureInfo.InvariantCulture);
                            dateStart = dt.ToString();
                        }
                    }
                    // end 

                    foreach (string itemData in fdata)
                    {
                        string value = itemData;
                        StreamReader fileReaderFolder = new StreamReader(value);
                        while (!fileReaderFolder.EndOfStream)
                        {
                            storeData = fileReaderFolder.ReadLine();
                            if (storeData.Contains("Date"))
                            {
                                string startTime = storeData;
                                string arraStartTime = startTime.Split('=').Last();
                                //var date = "11252017";
                                var date = DateTime.ParseExact(arraStartTime, "yyyyMMdd", CultureInfo.InvariantCulture);

                                timeValue = date;
                                CalendarData.AddBoldedDate(date);
                                CalendarData.UpdateBoldedDates();

                                CalendarData.SelectionStart = DateTime.Parse(dateStart);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        string dateCalc;
        public int selectRows;
        public int division;
        string path;
        /// <summary>
        /// called when the dates are highlighted 
        /// if teh user wants to see the items in the given date, 
        /// click the date, and if the date has beeen selected, the below f
        /// function will do the work.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void CalendarData_DateSelected(object sender, DateRangeEventArgs e)
        {
            try
            {
                //adding to list 
                lstData.Items.Clear();
                lstData.Items.Add(CalendarData.SelectionStart.ToString());
                dateCalc = CalendarData.SelectionStart.ToString();
                foreach (string itemData in fdata)
                {
                    string value = itemData;
                    StreamReader fileReaderFolder = new StreamReader(value);


                    while (!fileReaderFolder.EndOfStream)
                    {
                        storeData = fileReaderFolder.ReadLine();
                        if (storeData.Contains("Date"))
                        {
                            string startTime = storeData;
                            string arraStartTime = startTime.Split('=').Last();
                            string one = "";
                            //var date = "11252017";
                            var date = DateTime.ParseExact(arraStartTime, "yyyyMMdd", CultureInfo.InvariantCulture);

                            if (date == DateTime.Parse(dateCalc))
                            {
                                lstData.ClearSelected();
                                lstData.Items.Add(itemData.Split('\\').Last());

                                path = itemData;

                                lstData.Update();
                                //MessageBox.Show(one); 
                            }

                            

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lstData_MouseClick(object sender, MouseEventArgs e)
        {
            string filename = lstData.SelectedItem.ToString(); // name of the browsed file 
            string location = path;   // location of the browsed file
                                      //string fileData, fileDataTwo;
                                      //int count = 0;



            foreach (string item in fdata)
            {
                string data = item.Split('\\').Last();
                if (data == filename)
                {


                    fileReader(item);

                }
            }
        }


        string fileData;
        public string filename;
        string lengthValue, startTimeValue, intervalValue;

        int count = 0;
        IDictionary<string, string> param = new Dictionary<string, string>();
        public static double averageSpeed { get; set; }
        public static double maxSpeed { get; set; }
        public static double averageSpeedMiles { get; set; }
        public static double maxSpeedMiles { get; set; }
        public static double averageHeartRate { get; set; }
        public static double maxHeartRate { get; set; }
        public static double minHeartRate { get; set; }
        public static double averagePower { get; set; }
        public static double maxPower { get; set; }
        public static double averageAltitude { get; set; }
        public static double maxAltitude { get; set; }
        public static double averageAltitudeMile { get; set; }
        public static double maxAltitudeMile { get; set; }
        public static double totalDistance { get; set; }
        public static double totalDistanceMiles { get; set; }
        public static string smode { get; set; }

        int timeArrCount = 0;
        public static List<TimeSpan> totalTime = new List<TimeSpan>();
        TimeSpan startTime, endTime;

        public void intervalDetaction()
        {
            double threholdPowVal = Math.Round((105 * ftpGlobal) / 100, 2);
            int powerDown = 1;
            int powerUp = 1;
            double intervalValue = 0;
            try
            {
                foreach (double powerDataV in powerData)
                {
                    if (threholdPowVal >= powerDataV)
                    {
                        powerDown = 1;
                    }
                    if (powerDown == 1)
                    {
                        if (threholdPowVal <= powerDataV)
                            powerUp = 1;
                    }
                    if (powerUp == 1)
                    {
                        intervalValue++;
                        powerUp = 0;
                        powerDown = 0;
                    }
                    intervalDetectionData.Add(intervalValue);
                    powerInterval.Add(powerDataV);
                    threholdValueGlobal = threholdPowVal;

                    //MessageBox.Show(intervalValue.ToString() + " "+threholdPowVal + " " + powerDataV);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Some errors ocurred \n " + ex);
            }


        }
        public void fileReader(string data)
        {
            StreamReader fileReader = new StreamReader(data);


            try
            {
                while (!fileReader.EndOfStream)
                {
                    fileData = fileReader.ReadLine();
                    if (fileData.Contains("StartTime"))
                    {
                        string startTime = fileData;
                        string[] arraStartTime = startTime.Split('=');
                        foreach (String item in arraStartTime)
                        {
                           // lblStartTime.Text = item;
                            startTimeValue = item;
                        }
                    }
                    if (fileData.Contains("Interval"))
                    {
                        string interval = fileData;
                        string[] arrayInterval = interval.Split('=');
                        foreach (String itemInterval in arrayInterval)
                        {
                            //lblInterval.Text = itemInterval;
                        }
                    }
                    if (fileData.Contains("Weight"))
                    {
                        string weight = fileData;
                        string[] arrayWeight = weight.Split('=');
                        foreach (String itemWeight in arrayWeight)
                        {
                            //lblWeight.Text = itemWeight;
                        }
                    }
                    if (fileData.Contains("Length"))
                    {
                        string length = fileData;
                        string[] arrayInterval = length.Split('=');
                        foreach (String itemLength in arrayInterval)
                        {
                            lengthValue = itemLength;

                        }
                    }
                    if (fileData.Contains("Interval"))
                    {
                        string interval = fileData;
                        string[] arrayInterval = interval.Split('=');
                        foreach (String itemLength in arrayInterval)
                        {
                            intervalValue = itemLength;
                           // lblInterval.Text = itemLength;
                        }
                    }
                    if (fileData.Contains("Version"))
                    {
                        string version = fileData;
                        string[] arrayVersion = version.Split('=');
                        foreach (String itemVersion in arrayVersion)
                        {

                           // lblVersion.Text = itemVersion;
                        }
                    }
                    if (fileData.Contains("Monitor"))
                    {
                        string monitor = fileData;
                        string[] arrayMonitor = monitor.Split('=');
                        foreach (String itemMonitor in arrayMonitor)
                        {

                           // lblMonitor.Text = itemMonitor;
                        }
                    }
                    if (fileData.Contains("ActiveLimit"))
                    {
                        string activeLimit = fileData;
                        string[] arrayActiveLimit = activeLimit.Split('=');
                        foreach (String itemActiveLimit in arrayActiveLimit)
                        {

                        }
                    }
                    if (fileData.Contains("MaxHR"))
                    {
                        string maxHr = fileData;
                        string[] arrayMaxHr = maxHr.Split('=');
                        foreach (String itemMaxHr in arrayMaxHr)
                        {

                            //txtMaxHr.Text = itemMaxHr;
                        }
                    }
                    if (fileData.Contains("RestHR"))
                    {
                        string resetHr = fileData;
                        string[] arrayResetHr = resetHr.Split('=');
                        foreach (String itemResetHr in arrayResetHr)
                        {

                            // txtRestHr.Text = itemResetHr;
                        }
                    }
                    if (fileData.Contains("StartDelay"))
                    {
                        string startDelay = fileData;
                        string[] arrayStartDelay = startDelay.Split('=');
                        foreach (String itemStartDelay in arrayStartDelay)
                        {

                            //txtStartDelay.Text = itemStartDelay;
                        }
                    }
                    if (fileData.Contains("VO2max"))
                    {
                        string VO2max = fileData;
                        string[] arrayVO2max = VO2max.Split('=');
                        foreach (String itemVO2max in arrayVO2max)
                        {

                            //lblVersion.Text = itemVO2max;
                        }
                    }
                    if (fileData.Contains("Date"))
                    {
                        string Date = fileData;
                        string[] arrayDate = Date.Split('=');
                        char[] diff = arrayDate[1].ToCharArray();
                        foreach (String itemDate in arrayDate)
                        {
                            dateFinal = diff[0].ToString() + diff[1].ToString() + diff[2].ToString() + diff[3].ToString() + "/" + diff[4].ToString() + diff[5].ToString() + "/" + diff[6].ToString() + diff[7].ToString();
                           // lblDate.Text = dateFinal;
                        }

                    }
                    if (fileData.Contains("SMode"))
                    {
                        string smodeValue = fileData;
                        string[] arraySmode = smodeValue.Split('=');
                        foreach (String itemSmode in arraySmode)
                        {

                          //  smode = itemSmode;
                        }
                    }
                }

                List<List<string>> hrData = File.ReadLines(data)
                                           .SkipWhile(line => line != "[HRData]")
                                           .Skip(1)
                                           .Select(line => line.Split().ToList())
                                           .ToList();
                count = hrData.Count();

                //variable decarations 
                //calculation of rates 
                double speedTotal = 0;
                double heartRateTotal = 0;
                double powerTotal = 0;
                double altitudeTotal = 0;
                double[] arraySpeed = new double[500000];
                double[] arrayHeartRate = new double[500000];
                double[] arrayPower = new double[500000];
                double[] arrayAltitude = new double[500000];
                double[] arrayCadence = new double[500000];
                string[] arrayLength = new string[500000];
                string[] arrayStartTime = new string[500000];
                double intervalResult = 0;

                //calculation of timeinterval
                // time interval 
                arrayStartTime = startTimeValue.Split(':');
                string hour = arrayStartTime[0];
                string minute = arrayStartTime[1];
                double sec = double.Parse(arrayStartTime[2]);
                double min = double.Parse(arrayStartTime[0]);
                double hrs = double.Parse(arrayStartTime[1]);
                double intervalTwo = 0;
                for (int i = 0; i < count; i++)
                {
                    timeArrCount++;
                    double interval = double.Parse(intervalValue);

                    //sec = sec + interval ; 

                    intervalTwo = intervalTwo + interval;


                    dataGridView1.Rows.Add();
                    // dataGridView1.Rows[i].Cells[0].Value = dateFinal+"   |   "+ hour + ":" + minute + ":" + sec;
                    DateTime timer = DateTime.ParseExact(startTimeValue, "HH:mm:ss.FFF", CultureInfo.InvariantCulture);
                    dataGridView1.Rows[i].Cells[0].Value = dateFinal + " | " + timer.AddSeconds(intervalTwo).TimeOfDay;
                    //totalTime[i] = timer.AddSeconds(intervalTwo).TimeOfDay; 
                    totalTime.Add(timer.AddSeconds(intervalTwo).TimeOfDay);

                    int clm = 0;
                    char[] smodeData = smode.ToCharArray();
                    char speed = smodeData[0];
                    char cadence = smodeData[1];
                    char altitude = smodeData[2];
                    char power = smodeData[3];
                    char powerLRBalance = smodeData[4];
                    char PowerPIndex = smodeData[5];
                    char hrcc = smodeData[6];
                    char usEuroUnit = smodeData[7];
                    char airPressure = smodeData[8];
                    
                    if (hrcc == '1')
                    {
                        dataGridView1.Rows[i].Cells[1].Value = hrData[i][0];
                    }
                    else if (hrcc == '0')
                    {
                        dataGridView1.Rows[i].Cells[1].Value = 0;
                    }
                    if (speed == '1')
                    {
                        dataGridView1.Rows[i].Cells[2].Value = hrData[i][1];
                    }
                    else if (speed == '0')
                    {
                        dataGridView1.Rows[i].Cells[2].Value = 0;
                    }
                    if (cadence == '1')
                    {
                        dataGridView1.Rows[i].Cells[3].Value = hrData[i][2];
                    }
                    else if (cadence == '0')
                    {
                        dataGridView1.Rows[i].Cells[3].Value = 0;
                    }

                    if (altitude == '1')
                    {
                        dataGridView1.Rows[i].Cells[4].Value = hrData[i][3];
                    }
                    else if (altitude == '0')
                    {
                        dataGridView1.Rows[i].Cells[4].Value = 0;
                    }
                    if (power == '1')
                    {
                        dataGridView1.Rows[i].Cells[5].Value = hrData[i][4];
                        powerData.Add(Convert.ToDouble(hrData[i][4]));
                    }

                    else if (power == '0')
                    {
                        dataGridView1.Rows[i].Cells[5].Value = 0;
                    }
                    if (powerLRBalance == '1')
                    {
                        dataGridView1.Rows[i].Cells[6].Value = hrData[i][5];
                        double val = Convert.ToDouble(hrData[i][5]); // calculation of PI and LRB
                        double pi = val / 256;
                        double lrb = val % 256;
                        double rb = 100 - lrb;
                        dataGridView1.Rows[i].Cells[7].Value = Math.Round(pi, 0);
                        dataGridView1.Rows[i].Cells[8].Value = "L" + lrb + "- R" + rb;
                    }
                    else if (powerLRBalance == '0')
                    {
                        dataGridView1.Rows[i].Cells[6].Value = 0;
                    }
                    if (speed == '1')
                    {

                        // cadence 

                        arrayCadence[i] = int.Parse(hrData[i][2]);


                        // average speed 

                        speedTotal = speedTotal + int.Parse(hrData[i][1]);
                        averageSpeed = (speedTotal / count) * 0.1;
                        averageSpeedMiles = averageSpeed / 1.6;



                        // maximum speed  

                        arraySpeed[i] = int.Parse(hrData[i][1]);
                    }
                    else
                    {
                        averageSpeed = 0;
                        averageSpeedMiles = 0;
                        arraySpeed[i] = 0;

                    }

                    if (hrcc == '1')
                    {
                        // average heart rate 
                        heartRateTotal = heartRateTotal + int.Parse(hrData[i][0]);
                        averageHeartRate = heartRateTotal / count;
                        // maximum heart rate
                        arrayHeartRate[i] = int.Parse(hrData[i][0]);
                    }
                    else
                    {
                        averageHeartRate = 0;
                        arrayHeartRate[i] = 0;
                    }
                    if (power == '1')
                    {
                        // average power 
                        powerTotal = powerTotal + int.Parse(hrData[i][4]);
                        averagePower = powerTotal / count;
                        avgPowerGlobal = Math.Round(averagePower, 2);
                        // maximum power 
                        arrayPower[i] = int.Parse(hrData[i][4]);
                    }
                    else
                    {
                        averagePower = 0;
                        arrayPower[i] = 0;
                    }
                    if (altitude == '1')
                    {
                        // average altitude 
                        altitudeTotal = altitudeTotal + int.Parse(hrData[i][3]);
                        averageAltitude = altitudeTotal / count;
                        averageAltitudeMile = averageAltitude / 0.3048;
                        // maximum altitude 
                        arrayAltitude[i] = int.Parse(hrData[i][3]);
                    }
                    else
                    {
                        averageAltitude = 0;
                        averageAltitudeMile = 0;
                        arrayAltitude[i] = 0;
                    }
                }
                // normalization power 

                // calculation of moving average 
                int value = powerData.Count();
                // movAvgCount = value;
                //  MessageBox.Show(value.ToString());

                for (int x = 0; x < value; x++)
                {
                    double movingAverage30 = 0;
                    for (int j = 0; j < 30; j++)
                    {
                        int index = x + j;
                        index %= value;
                        movingAverage30 += Convert.ToDouble(powerData[index]);
                    }

                    movingAverage30 /= 30;

                    double movAvgPow = Math.Pow(movingAverage30, 4);
                    movAvgPow4.Add(movAvgPow);
                    movAvg.Add(movingAverage30);

                }


                // MessageBox.Show(movAvgCount.ToString());
                movAvgCount = movAvgPow4.Count();
                if (movAvgPow4 != null)
                {
                    double movAvgPow4Sum = movAvgPow4.Sum();
                    double power = movAvgPow4Sum / movAvgCount;
                    double normalizationPower = Math.Round(Math.Pow(power, 1.0 / 4), 2);
                    double movingAverageSum = movAvg.Sum();
                    double movingAverageValue = movingAverageSum / movAvgCount; // moving average value 
                                                                                // movingAverageGlobal = movingAverageValue;  
                    normalizationPowerGlobal = normalizationPower;
                    // ftp value 
                    double ftpData = 0.95 * avgPowerGlobal;
                    ftpGlobal = ftpData;
                    ifGlobal = normalizationPowerGlobal / ftpGlobal;
                    // for tss 

                    startTime = totalTime.First();
                    endTime = totalTime.Last();
                    double startTimeSec = startTime.TotalSeconds;
                    double endTimeSec = endTime.TotalSeconds;
                    TimeSpan length = TimeSpan.Parse(lengthValue);
                    double lengthToSec = length.TotalSeconds;
                    double totalTimeDurationSec = lengthToSec;

                    double tssGlobalOne = normalizationPowerGlobal * ifGlobal * totalTimeDurationSec; // sec value left  
                    double tssGlobalTwo = ftpGlobal * 3600;
                    double tssGlobalThree = tssGlobalOne / tssGlobalTwo;
                    double tssGlobalFour = tssGlobalThree * 100;
                    tssGlobal = tssGlobalFour;   // calculating tss 
                                                 // MessageBox.Show(ftpData.ToString()); 

                    // string totalTimeDuration = TimeSpan.FromDays(totalTimeDurationSec).ToString(@"dd\:hh\:mm");

                    double threholdPowVal = Math.Round((105 * ftpGlobal) / 100, 2);
                    // double thresholdPowResul = ftpGlobal - threholdPowVal;
                    int intervalCountUp = 0;
                    int intervalCountDown = 0;
                    List<double> chk = new List<double>();



                    for (int v = 0; v < powerData.Count; v++)
                    {
                        if (powerData[v] >= threholdPowVal)
                        {
                            intervalCountUp = v;
                            chk.Add(v);

                            // MessageBox.Show(intervalCountUp.ToString());
                        }
                        if (powerData[v] <= threholdPowVal)
                        {
                            intervalCountDown = v;
                            chk.Add(v);

                            // MessageBox.Show(intervalCountDown.ToString());
                        }
                    }
                    intervalDetaction();
                }

                maxSpeed = arraySpeed.Max() * 0.1;
                maxSpeedMiles = (maxSpeed) / 1.6;

                //max heart rate 
                maxHeartRate = arrayHeartRate.Max();


                // min heart rate 
                // minHeartRate = arrayHeartRate.Min();
                minHeartRate = double.MaxValue;


                foreach (double valueHR in arrayHeartRate)
                {
                    double num = valueHR;
                    if (num < minHeartRate)
                        minHeartRate = num;
                }
                // max power 
                maxPower = arrayPower.Max();
                // max altitude 
                maxAltitude = arrayAltitude.Max();
                maxAltitudeMile = maxAltitude / 0.3048;

                // total distance covered 
                if (arrayLength != null)
                {
                    arrayLength = lengthValue.Split(':');
                    double hourDis = double.Parse(arrayLength[0]) * 3600;
                    double minDis = double.Parse(arrayLength[1]) * 60;
                    double secDis = double.Parse(arrayLength[2]);

                    double length = hourDis + minDis + secDis;
                    double lengthFinal = length / 3600;
                    double totalDistanceProcess = averageSpeed * lengthFinal;
                    double totalDistanceProcessMiles = (totalDistanceProcess) / 1.6;
                    totalDistance = Math.Round(totalDistanceProcess, 2);
                    totalDistanceMiles = Math.Round(totalDistanceProcessMiles, 2); ;

                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Some errors ocurred \n " + ex);
            }

        }
    }
    }

