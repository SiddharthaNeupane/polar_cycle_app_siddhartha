﻿namespace Polar_Cycle_App_Siddhartha
{
    partial class ChunkData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.inputValue = new System.Windows.Forms.ComboBox();
            this.OK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(149, 180);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Input the number";
            // 
            // inputValue
            // 
            this.inputValue.FormattingEnabled = true;
            this.inputValue.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.inputValue.Location = new System.Drawing.Point(255, 177);
            this.inputValue.Name = "inputValue";
            this.inputValue.Size = new System.Drawing.Size(176, 21);
            this.inputValue.TabIndex = 1;
            this.inputValue.SelectedIndexChanged += new System.EventHandler(this.inputValue_SelectedIndexChanged);
            // 
            // OK
            // 
            this.OK.Location = new System.Drawing.Point(255, 257);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(75, 23);
            this.OK.TabIndex = 2;
            this.OK.Text = "OK";
            this.OK.UseVisualStyleBackColor = true;
            this.OK.Click += new System.EventHandler(this.OK_Click);
            // 
            // ChunkData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 436);
            this.Controls.Add(this.OK);
            this.Controls.Add(this.inputValue);
            this.Controls.Add(this.label1);
            this.Name = "ChunkData";
            this.Text = "Select the number to  divide selected rows to chunks";
            this.Load += new System.EventHandler(this.ChunkData_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox inputValue;
        private System.Windows.Forms.Button OK;
    }
}