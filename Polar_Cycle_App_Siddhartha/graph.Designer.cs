﻿namespace Polar_Cycle_App_Siddhartha
{
    partial class graph
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.chkPower = new MetroFramework.Controls.MetroCheckBox();
            this.chkAltitude = new MetroFramework.Controls.MetroCheckBox();
            this.chkCadence = new MetroFramework.Controls.MetroCheckBox();
            this.chkSpeed = new MetroFramework.Controls.MetroCheckBox();
            this.chkHeartRate = new MetroFramework.Controls.MetroCheckBox();
            this.SuspendLayout();
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.IsShowPointValues = false;
            this.zedGraphControl1.Location = new System.Drawing.Point(246, 264);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.PointValueFormat = "G";
            this.zedGraphControl1.Size = new System.Drawing.Size(819, 368);
            this.zedGraphControl1.TabIndex = 0;
            this.zedGraphControl1.Load += new System.EventHandler(this.zedGraphControl1_Load);
            // 
            // chkPower
            // 
            this.chkPower.AutoSize = true;
            this.chkPower.Checked = true;
            this.chkPower.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPower.Location = new System.Drawing.Point(874, 655);
            this.chkPower.Name = "chkPower";
            this.chkPower.Size = new System.Drawing.Size(56, 15);
            this.chkPower.TabIndex = 2;
            this.chkPower.Text = "Power";
            this.chkPower.UseSelectable = true;
            this.chkPower.CheckedChanged += new System.EventHandler(this.chkPower_CheckedChanged);
            // 
            // chkAltitude
            // 
            this.chkAltitude.AutoSize = true;
            this.chkAltitude.Checked = true;
            this.chkAltitude.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAltitude.Location = new System.Drawing.Point(767, 655);
            this.chkAltitude.Name = "chkAltitude";
            this.chkAltitude.Size = new System.Drawing.Size(65, 15);
            this.chkAltitude.TabIndex = 3;
            this.chkAltitude.Text = "Altitude";
            this.chkAltitude.UseSelectable = true;
            this.chkAltitude.CheckedChanged += new System.EventHandler(this.chkAltitude_CheckedChanged);
            // 
            // chkCadence
            // 
            this.chkCadence.AutoSize = true;
            this.chkCadence.Checked = true;
            this.chkCadence.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCadence.Location = new System.Drawing.Point(658, 655);
            this.chkCadence.Name = "chkCadence";
            this.chkCadence.Size = new System.Drawing.Size(69, 15);
            this.chkCadence.TabIndex = 4;
            this.chkCadence.Text = "Cadence";
            this.chkCadence.UseSelectable = true;
            this.chkCadence.CheckedChanged += new System.EventHandler(this.chkCadence_CheckedChanged);
            // 
            // chkSpeed
            // 
            this.chkSpeed.AutoSize = true;
            this.chkSpeed.Checked = true;
            this.chkSpeed.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSpeed.Location = new System.Drawing.Point(539, 655);
            this.chkSpeed.Name = "chkSpeed";
            this.chkSpeed.Size = new System.Drawing.Size(55, 15);
            this.chkSpeed.TabIndex = 5;
            this.chkSpeed.Text = "Speed";
            this.chkSpeed.UseSelectable = true;
            this.chkSpeed.CheckedChanged += new System.EventHandler(this.chkSpeed_CheckedChanged);
            // 
            // chkHeartRate
            // 
            this.chkHeartRate.AutoSize = true;
            this.chkHeartRate.Checked = true;
            this.chkHeartRate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkHeartRate.Location = new System.Drawing.Point(427, 655);
            this.chkHeartRate.Name = "chkHeartRate";
            this.chkHeartRate.Size = new System.Drawing.Size(78, 15);
            this.chkHeartRate.TabIndex = 6;
            this.chkHeartRate.Text = "Heart Rate";
            this.chkHeartRate.UseSelectable = true;
            this.chkHeartRate.CheckedChanged += new System.EventHandler(this.chkHeartRate_CheckedChanged);
            // 
            // graph
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1215, 710);
            this.Controls.Add(this.chkPower);
            this.Controls.Add(this.chkAltitude);
            this.Controls.Add(this.chkCadence);
            this.Controls.Add(this.chkSpeed);
            this.Controls.Add(this.chkHeartRate);
            this.Controls.Add(this.zedGraphControl1);
            this.Name = "graph";
            this.Text = "Graph";
            this.Load += new System.EventHandler(this.graph_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ZedGraph.ZedGraphControl zedGraphControl1;
        private MetroFramework.Controls.MetroCheckBox chkPower;
        private MetroFramework.Controls.MetroCheckBox chkAltitude;
        private MetroFramework.Controls.MetroCheckBox chkCadence;
        private MetroFramework.Controls.MetroCheckBox chkSpeed;
        private MetroFramework.Controls.MetroCheckBox chkHeartRate;
    }
}