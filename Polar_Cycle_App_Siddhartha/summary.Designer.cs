﻿namespace Polar_Cycle_App_Siddhartha
{
    partial class summary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.radioMiles = new MetroFramework.Controls.MetroRadioButton();
            this.radioKm = new MetroFramework.Controls.MetroRadioButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.lblTotalDistance = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.lblMaximumSpeed = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.lblMinimumHeartRate = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.lblAveragePower = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.lblMaximumPower = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.lblAverageAltitude = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.lblMaximumHeartRate = new MetroFramework.Controls.MetroLabel();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.lblAverageSpeed = new MetroFramework.Controls.MetroLabel();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.lblAverageHeartRate = new MetroFramework.Controls.MetroLabel();
            this.metroLabel20 = new MetroFramework.Controls.MetroLabel();
            this.lblMaximumAltitude = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.Gray;
            this.metroPanel1.Controls.Add(this.radioMiles);
            this.metroPanel1.Controls.Add(this.radioKm);
            this.metroPanel1.Controls.Add(this.metroLabel1);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(523, 120);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(210, 174);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // radioMiles
            // 
            this.radioMiles.AutoSize = true;
            this.radioMiles.BackColor = System.Drawing.Color.Transparent;
            this.radioMiles.Location = new System.Drawing.Point(84, 122);
            this.radioMiles.Name = "radioMiles";
            this.radioMiles.Size = new System.Drawing.Size(46, 15);
            this.radioMiles.TabIndex = 3;
            this.radioMiles.Text = "Mile";
            this.radioMiles.UseSelectable = true;
            this.radioMiles.CheckedChanged += new System.EventHandler(this.radioMiles_CheckedChanged);
            // 
            // radioKm
            // 
            this.radioKm.AutoSize = true;
            this.radioKm.BackColor = System.Drawing.Color.Transparent;
            this.radioKm.Location = new System.Drawing.Point(84, 84);
            this.radioKm.Name = "radioKm";
            this.radioKm.Size = new System.Drawing.Size(57, 15);
            this.radioKm.TabIndex = 3;
            this.radioKm.Text = "Km/hr";
            this.radioKm.UseSelectable = true;
            this.radioKm.CheckedChanged += new System.EventHandler(this.radioKm_CheckedChanged);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.ForeColor = System.Drawing.Color.Black;
            this.metroLabel1.Location = new System.Drawing.Point(56, 27);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(101, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Unit Conversion";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(111, 80);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(142, 19);
            this.metroLabel2.TabIndex = 1;
            this.metroLabel2.Text = "Total Distance Covered";
            // 
            // lblTotalDistance
            // 
            this.lblTotalDistance.AutoSize = true;
            this.lblTotalDistance.Location = new System.Drawing.Point(347, 80);
            this.lblTotalDistance.Name = "lblTotalDistance";
            this.lblTotalDistance.Size = new System.Drawing.Size(15, 19);
            this.lblTotalDistance.TabIndex = 1;
            this.lblTotalDistance.Text = "-";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(111, 137);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(109, 19);
            this.metroLabel4.TabIndex = 1;
            this.metroLabel4.Text = "Maximum Speed";
            // 
            // lblMaximumSpeed
            // 
            this.lblMaximumSpeed.AutoSize = true;
            this.lblMaximumSpeed.Location = new System.Drawing.Point(347, 137);
            this.lblMaximumSpeed.Name = "lblMaximumSpeed";
            this.lblMaximumSpeed.Size = new System.Drawing.Size(15, 19);
            this.lblMaximumSpeed.TabIndex = 1;
            this.lblMaximumSpeed.Text = "-";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(111, 369);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(132, 19);
            this.metroLabel6.TabIndex = 1;
            this.metroLabel6.Text = "Minimum Heart Rate";
            // 
            // lblMinimumHeartRate
            // 
            this.lblMinimumHeartRate.AutoSize = true;
            this.lblMinimumHeartRate.Location = new System.Drawing.Point(347, 369);
            this.lblMinimumHeartRate.Name = "lblMinimumHeartRate";
            this.lblMinimumHeartRate.Size = new System.Drawing.Size(15, 19);
            this.lblMinimumHeartRate.TabIndex = 1;
            this.lblMinimumHeartRate.Text = "-";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(111, 229);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(102, 19);
            this.metroLabel8.TabIndex = 1;
            this.metroLabel8.Text = "Average Power ";
            // 
            // lblAveragePower
            // 
            this.lblAveragePower.AutoSize = true;
            this.lblAveragePower.Location = new System.Drawing.Point(347, 229);
            this.lblAveragePower.Name = "lblAveragePower";
            this.lblAveragePower.Size = new System.Drawing.Size(15, 19);
            this.lblAveragePower.TabIndex = 1;
            this.lblAveragePower.Text = "-";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(111, 265);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(108, 19);
            this.metroLabel10.TabIndex = 1;
            this.metroLabel10.Text = "Maximum Power";
            // 
            // lblMaximumPower
            // 
            this.lblMaximumPower.AutoSize = true;
            this.lblMaximumPower.Location = new System.Drawing.Point(347, 265);
            this.lblMaximumPower.Name = "lblMaximumPower";
            this.lblMaximumPower.Size = new System.Drawing.Size(15, 19);
            this.lblMaximumPower.TabIndex = 1;
            this.lblMaximumPower.Text = "-";
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(111, 299);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(107, 19);
            this.metroLabel12.TabIndex = 1;
            this.metroLabel12.Text = "Average Altitude";
            // 
            // lblAverageAltitude
            // 
            this.lblAverageAltitude.AutoSize = true;
            this.lblAverageAltitude.Location = new System.Drawing.Point(347, 299);
            this.lblAverageAltitude.Name = "lblAverageAltitude";
            this.lblAverageAltitude.Size = new System.Drawing.Size(15, 19);
            this.lblAverageAltitude.TabIndex = 1;
            this.lblAverageAltitude.Text = "-";
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(111, 201);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(135, 19);
            this.metroLabel14.TabIndex = 1;
            this.metroLabel14.Text = "Maximum Heart Rate";
            // 
            // lblMaximumHeartRate
            // 
            this.lblMaximumHeartRate.AutoSize = true;
            this.lblMaximumHeartRate.Location = new System.Drawing.Point(347, 201);
            this.lblMaximumHeartRate.Name = "lblMaximumHeartRate";
            this.lblMaximumHeartRate.Size = new System.Drawing.Size(15, 19);
            this.lblMaximumHeartRate.TabIndex = 1;
            this.lblMaximumHeartRate.Text = "-";
            // 
            // metroLabel16
            // 
            this.metroLabel16.AutoSize = true;
            this.metroLabel16.Location = new System.Drawing.Point(111, 110);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(99, 19);
            this.metroLabel16.TabIndex = 1;
            this.metroLabel16.Text = "Average Speed";
            // 
            // lblAverageSpeed
            // 
            this.lblAverageSpeed.AutoSize = true;
            this.lblAverageSpeed.Location = new System.Drawing.Point(347, 110);
            this.lblAverageSpeed.Name = "lblAverageSpeed";
            this.lblAverageSpeed.Size = new System.Drawing.Size(15, 19);
            this.lblAverageSpeed.TabIndex = 1;
            this.lblAverageSpeed.Text = "-";
            // 
            // metroLabel18
            // 
            this.metroLabel18.AutoSize = true;
            this.metroLabel18.Location = new System.Drawing.Point(111, 170);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(125, 19);
            this.metroLabel18.TabIndex = 1;
            this.metroLabel18.Text = "Average Heart Rate";
            // 
            // lblAverageHeartRate
            // 
            this.lblAverageHeartRate.AutoSize = true;
            this.lblAverageHeartRate.Location = new System.Drawing.Point(347, 170);
            this.lblAverageHeartRate.Name = "lblAverageHeartRate";
            this.lblAverageHeartRate.Size = new System.Drawing.Size(15, 19);
            this.lblAverageHeartRate.TabIndex = 1;
            this.lblAverageHeartRate.Text = "-";
            // 
            // metroLabel20
            // 
            this.metroLabel20.AutoSize = true;
            this.metroLabel20.Location = new System.Drawing.Point(111, 335);
            this.metroLabel20.Name = "metroLabel20";
            this.metroLabel20.Size = new System.Drawing.Size(117, 19);
            this.metroLabel20.TabIndex = 1;
            this.metroLabel20.Text = "Maximum Altitude";
            // 
            // lblMaximumAltitude
            // 
            this.lblMaximumAltitude.AutoSize = true;
            this.lblMaximumAltitude.Location = new System.Drawing.Point(347, 335);
            this.lblMaximumAltitude.Name = "lblMaximumAltitude";
            this.lblMaximumAltitude.Size = new System.Drawing.Size(15, 19);
            this.lblMaximumAltitude.TabIndex = 1;
            this.lblMaximumAltitude.Text = "-";
            // 
            // summary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 438);
            this.Controls.Add(this.lblMaximumAltitude);
            this.Controls.Add(this.lblMaximumHeartRate);
            this.Controls.Add(this.lblAverageAltitude);
            this.Controls.Add(this.lblMaximumPower);
            this.Controls.Add(this.lblAveragePower);
            this.Controls.Add(this.lblAverageHeartRate);
            this.Controls.Add(this.lblMinimumHeartRate);
            this.Controls.Add(this.lblMaximumSpeed);
            this.Controls.Add(this.lblAverageSpeed);
            this.Controls.Add(this.metroLabel20);
            this.Controls.Add(this.lblTotalDistance);
            this.Controls.Add(this.metroLabel14);
            this.Controls.Add(this.metroLabel12);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.metroLabel18);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel16);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroPanel1);
            this.Name = "summary";
            this.Text = "Data Summary";
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroRadioButton radioMiles;
        private MetroFramework.Controls.MetroRadioButton radioKm;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel lblTotalDistance;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel lblMaximumSpeed;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel lblMinimumHeartRate;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel lblAveragePower;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel lblMaximumPower;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroLabel lblAverageAltitude;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroLabel lblMaximumHeartRate;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private MetroFramework.Controls.MetroLabel lblAverageSpeed;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private MetroFramework.Controls.MetroLabel lblAverageHeartRate;
        private MetroFramework.Controls.MetroLabel metroLabel20;
        private MetroFramework.Controls.MetroLabel lblMaximumAltitude;
    }
}