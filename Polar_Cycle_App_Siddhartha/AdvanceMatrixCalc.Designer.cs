﻿namespace Polar_Cycle_App_Siddhartha
{
    partial class AdvanceMatrixCalc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.lblMovAvg = new MetroFramework.Controls.MetroLabel();
            this.lblNorPow = new MetroFramework.Controls.MetroLabel();
            this.lblFuncThres = new MetroFramework.Controls.MetroLabel();
            this.lblIntesiFac = new MetroFramework.Controls.MetroLabel();
            this.lblTraiStres = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(212, 129);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(109, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Moving Average:";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(203, 185);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(118, 19);
            this.metroLabel2.TabIndex = 0;
            this.metroLabel2.Text = "Normalized Power";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(226, 267);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(95, 19);
            this.metroLabel3.TabIndex = 0;
            this.metroLabel3.Text = "Intensity Factor";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(139, 230);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(182, 19);
            this.metroLabel4.TabIndex = 0;
            this.metroLabel4.Text = "Functionality Threshold Power";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(197, 310);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(124, 19);
            this.metroLabel5.TabIndex = 0;
            this.metroLabel5.Text = "Training Stress Core";
            // 
            // lblMovAvg
            // 
            this.lblMovAvg.AutoSize = true;
            this.lblMovAvg.Location = new System.Drawing.Point(403, 129);
            this.lblMovAvg.Name = "lblMovAvg";
            this.lblMovAvg.Size = new System.Drawing.Size(33, 19);
            this.lblMovAvg.TabIndex = 1;
            this.lblMovAvg.Text = "0.00";
            // 
            // lblNorPow
            // 
            this.lblNorPow.AutoSize = true;
            this.lblNorPow.Location = new System.Drawing.Point(403, 185);
            this.lblNorPow.Name = "lblNorPow";
            this.lblNorPow.Size = new System.Drawing.Size(33, 19);
            this.lblNorPow.TabIndex = 1;
            this.lblNorPow.Text = "0.00";
            // 
            // lblFuncThres
            // 
            this.lblFuncThres.AutoSize = true;
            this.lblFuncThres.Location = new System.Drawing.Point(403, 230);
            this.lblFuncThres.Name = "lblFuncThres";
            this.lblFuncThres.Size = new System.Drawing.Size(33, 19);
            this.lblFuncThres.TabIndex = 1;
            this.lblFuncThres.Text = "0.00";
            // 
            // lblIntesiFac
            // 
            this.lblIntesiFac.AutoSize = true;
            this.lblIntesiFac.Location = new System.Drawing.Point(403, 267);
            this.lblIntesiFac.Name = "lblIntesiFac";
            this.lblIntesiFac.Size = new System.Drawing.Size(33, 19);
            this.lblIntesiFac.TabIndex = 1;
            this.lblIntesiFac.Text = "0.00";
            // 
            // lblTraiStres
            // 
            this.lblTraiStres.AutoSize = true;
            this.lblTraiStres.Location = new System.Drawing.Point(403, 310);
            this.lblTraiStres.Name = "lblTraiStres";
            this.lblTraiStres.Size = new System.Drawing.Size(33, 19);
            this.lblTraiStres.TabIndex = 1;
            this.lblTraiStres.Text = "0.00";
            // 
            // AdvanceMatrixCalc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 404);
            this.Controls.Add(this.lblTraiStres);
            this.Controls.Add(this.lblIntesiFac);
            this.Controls.Add(this.lblFuncThres);
            this.Controls.Add(this.lblNorPow);
            this.Controls.Add(this.lblMovAvg);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Name = "AdvanceMatrixCalc";
            this.Text = "AdvanceMatrixCalc";
            this.Load += new System.EventHandler(this.AdvanceMatrixCalc_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel lblMovAvg;
        private MetroFramework.Controls.MetroLabel lblNorPow;
        private MetroFramework.Controls.MetroLabel lblFuncThres;
        private MetroFramework.Controls.MetroLabel lblIntesiFac;
        private MetroFramework.Controls.MetroLabel lblTraiStres;
    }
}