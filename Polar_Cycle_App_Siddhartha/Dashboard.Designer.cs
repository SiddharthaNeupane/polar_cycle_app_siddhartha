﻿namespace Polar_Cycle_App_Siddhartha
{
    partial class OpenFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelDetails = new MetroFramework.Controls.MetroPanel();
            this.lblInterval = new MetroFramework.Controls.MetroLabel();
            this.lblWeight = new MetroFramework.Controls.MetroLabel();
            this.lblSmode = new MetroFramework.Controls.MetroLabel();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.lblStartTime = new MetroFramework.Controls.MetroLabel();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.lblDate = new MetroFramework.Controls.MetroLabel();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.lblLength = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.lblMonitor = new MetroFramework.Controls.MetroLabel();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.lblVersion = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.btnLoadHRM = new MetroFramework.Controls.MetroButton();
            this.btnViewGraph = new MetroFramework.Controls.MetroButton();
            this.btnViewSummary = new MetroFramework.Controls.MetroButton();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.lblsmSpeed = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.lblsmPowBal = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.lblsmCad = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.lblsmAir = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.lblsmAltd = new MetroFramework.Controls.MetroLabel();
            this.lblsmUS = new MetroFramework.Controls.MetroLabel();
            this.lblsmPower = new MetroFramework.Controls.MetroLabel();
            this.lblsmHRCC = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.lblsmPowerIndex = new MetroFramework.Controls.MetroLabel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.TimeInterval = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HeartRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Speeds = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cadences = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Altitudes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PowerS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.powerBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PIS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lrds = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnadvancedMetrx = new MetroFramework.Controls.MetroButton();
            this.btnIntervalDetection = new MetroFramework.Controls.MetroButton();
            this.btnFileComparison = new MetroFramework.Controls.MetroButton();
            this.btnCalendar = new MetroFramework.Controls.MetroButton();
            this.panelDetails.SuspendLayout();
            this.metroPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelDetails
            // 
            this.panelDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panelDetails.Controls.Add(this.lblInterval);
            this.panelDetails.Controls.Add(this.lblWeight);
            this.panelDetails.Controls.Add(this.lblSmode);
            this.panelDetails.Controls.Add(this.metroLabel18);
            this.panelDetails.Controls.Add(this.metroLabel17);
            this.panelDetails.Controls.Add(this.lblStartTime);
            this.panelDetails.Controls.Add(this.metroLabel16);
            this.panelDetails.Controls.Add(this.lblDate);
            this.panelDetails.Controls.Add(this.metroLabel15);
            this.panelDetails.Controls.Add(this.lblLength);
            this.panelDetails.Controls.Add(this.metroLabel14);
            this.panelDetails.Controls.Add(this.lblMonitor);
            this.panelDetails.Controls.Add(this.metroLabel13);
            this.panelDetails.Controls.Add(this.lblVersion);
            this.panelDetails.Controls.Add(this.metroLabel12);
            this.panelDetails.Controls.Add(this.metroLabel11);
            this.panelDetails.Controls.Add(this.metroLabel1);
            this.panelDetails.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.panelDetails.HorizontalScrollbarBarColor = true;
            this.panelDetails.HorizontalScrollbarHighlightOnWheel = false;
            this.panelDetails.HorizontalScrollbarSize = 10;
            this.panelDetails.Location = new System.Drawing.Point(43, 166);
            this.panelDetails.Name = "panelDetails";
            this.panelDetails.Size = new System.Drawing.Size(230, 422);
            this.panelDetails.TabIndex = 1;
            this.panelDetails.VerticalScrollbarBarColor = true;
            this.panelDetails.VerticalScrollbarHighlightOnWheel = false;
            this.panelDetails.VerticalScrollbarSize = 10;
            // 
            // lblInterval
            // 
            this.lblInterval.AutoSize = true;
            this.lblInterval.Location = new System.Drawing.Point(136, 335);
            this.lblInterval.Name = "lblInterval";
            this.lblInterval.Size = new System.Drawing.Size(15, 19);
            this.lblInterval.TabIndex = 3;
            this.lblInterval.Text = "-";
            // 
            // lblWeight
            // 
            this.lblWeight.AutoSize = true;
            this.lblWeight.Location = new System.Drawing.Point(136, 300);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(15, 19);
            this.lblWeight.TabIndex = 3;
            this.lblWeight.Text = "-";
            // 
            // lblSmode
            // 
            this.lblSmode.AutoSize = true;
            this.lblSmode.Location = new System.Drawing.Point(136, 267);
            this.lblSmode.Name = "lblSmode";
            this.lblSmode.Size = new System.Drawing.Size(16, 19);
            this.lblSmode.TabIndex = 3;
            this.lblSmode.Text = "0";
            // 
            // metroLabel18
            // 
            this.metroLabel18.AutoSize = true;
            this.metroLabel18.Location = new System.Drawing.Point(50, 335);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(52, 19);
            this.metroLabel18.TabIndex = 2;
            this.metroLabel18.Text = "Interval";
            // 
            // metroLabel17
            // 
            this.metroLabel17.AutoSize = true;
            this.metroLabel17.Location = new System.Drawing.Point(51, 85);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(51, 19);
            this.metroLabel17.TabIndex = 2;
            this.metroLabel17.Text = "Version";
            // 
            // lblStartTime
            // 
            this.lblStartTime.AutoSize = true;
            this.lblStartTime.Location = new System.Drawing.Point(136, 228);
            this.lblStartTime.Name = "lblStartTime";
            this.lblStartTime.Size = new System.Drawing.Size(15, 19);
            this.lblStartTime.TabIndex = 3;
            this.lblStartTime.Text = "-";
            // 
            // metroLabel16
            // 
            this.metroLabel16.AutoSize = true;
            this.metroLabel16.Location = new System.Drawing.Point(50, 121);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(56, 19);
            this.metroLabel16.TabIndex = 2;
            this.metroLabel16.Text = "Monitor";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(136, 192);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(15, 19);
            this.lblDate.TabIndex = 3;
            this.lblDate.Text = "-";
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.Location = new System.Drawing.Point(32, 228);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(70, 19);
            this.metroLabel15.TabIndex = 2;
            this.metroLabel15.Text = "Start Time";
            // 
            // lblLength
            // 
            this.lblLength.AutoSize = true;
            this.lblLength.Location = new System.Drawing.Point(136, 157);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(33, 19);
            this.lblLength.TabIndex = 3;
            this.lblLength.Text = "0:00";
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(51, 267);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(51, 19);
            this.metroLabel14.TabIndex = 2;
            this.metroLabel14.Text = "SMode";
            // 
            // lblMonitor
            // 
            this.lblMonitor.AutoSize = true;
            this.lblMonitor.Location = new System.Drawing.Point(136, 121);
            this.lblMonitor.Name = "lblMonitor";
            this.lblMonitor.Size = new System.Drawing.Size(15, 19);
            this.lblMonitor.TabIndex = 3;
            this.lblMonitor.Text = "-";
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(54, 157);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(48, 19);
            this.metroLabel13.TabIndex = 2;
            this.metroLabel13.Text = "Length";
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(136, 85);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(15, 19);
            this.lblVersion.TabIndex = 3;
            this.lblVersion.Text = "-";
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(52, 300);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(50, 19);
            this.metroLabel12.TabIndex = 2;
            this.metroLabel12.Text = "Weight";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(66, 192);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(36, 19);
            this.metroLabel11.TabIndex = 2;
            this.metroLabel11.Text = "Date";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(72, 11);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(80, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "HRM Details";
            // 
            // btnLoadHRM
            // 
            this.btnLoadHRM.AutoSize = true;
            this.btnLoadHRM.Location = new System.Drawing.Point(50, 91);
            this.btnLoadHRM.Name = "btnLoadHRM";
            this.btnLoadHRM.Size = new System.Drawing.Size(143, 24);
            this.btnLoadHRM.TabIndex = 3;
            this.btnLoadHRM.Text = "Load Data";
            this.btnLoadHRM.UseSelectable = true;
            this.btnLoadHRM.Click += new System.EventHandler(this.btnLoadHRM_Click);
            // 
            // btnViewGraph
            // 
            this.btnViewGraph.AutoSize = true;
            this.btnViewGraph.Location = new System.Drawing.Point(348, 91);
            this.btnViewGraph.Name = "btnViewGraph";
            this.btnViewGraph.Size = new System.Drawing.Size(143, 24);
            this.btnViewGraph.TabIndex = 3;
            this.btnViewGraph.Text = "View Graph";
            this.btnViewGraph.UseSelectable = true;
            this.btnViewGraph.Click += new System.EventHandler(this.btnViewGraph_Click);
            // 
            // btnViewSummary
            // 
            this.btnViewSummary.AutoSize = true;
            this.btnViewSummary.Location = new System.Drawing.Point(199, 91);
            this.btnViewSummary.Name = "btnViewSummary";
            this.btnViewSummary.Size = new System.Drawing.Size(143, 24);
            this.btnViewSummary.TabIndex = 3;
            this.btnViewSummary.Text = "View Summary";
            this.btnViewSummary.UseSelectable = true;
            this.btnViewSummary.Click += new System.EventHandler(this.btnViewSummary_Click);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(73, 192);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(45, 19);
            this.metroLabel6.TabIndex = 2;
            this.metroLabel6.Text = "Power";
            // 
            // lblsmSpeed
            // 
            this.lblsmSpeed.AutoSize = true;
            this.lblsmSpeed.Location = new System.Drawing.Point(169, 85);
            this.lblsmSpeed.Name = "lblsmSpeed";
            this.lblsmSpeed.Size = new System.Drawing.Size(15, 19);
            this.lblsmSpeed.TabIndex = 3;
            this.lblsmSpeed.Text = "-";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(43, 300);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(75, 19);
            this.metroLabel9.TabIndex = 2;
            this.metroLabel9.Text = "AirPressure";
            // 
            // lblsmPowBal
            // 
            this.lblsmPowBal.AutoSize = true;
            this.lblsmPowBal.Location = new System.Drawing.Point(169, 262);
            this.lblsmPowBal.Name = "lblsmPowBal";
            this.lblsmPowBal.Size = new System.Drawing.Size(15, 19);
            this.lblsmPowBal.TabIndex = 3;
            this.lblsmPowBal.Text = "-";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(64, 157);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(54, 19);
            this.metroLabel5.TabIndex = 2;
            this.metroLabel5.Text = "Altitude";
            // 
            // lblsmCad
            // 
            this.lblsmCad.AutoSize = true;
            this.lblsmCad.Location = new System.Drawing.Point(169, 121);
            this.lblsmCad.Name = "lblsmCad";
            this.lblsmCad.Size = new System.Drawing.Size(15, 19);
            this.lblsmCad.TabIndex = 3;
            this.lblsmCad.Text = "-";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(24, 267);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(94, 19);
            this.metroLabel8.TabIndex = 2;
            this.metroLabel8.Text = "Power Balance";
            // 
            // lblsmAir
            // 
            this.lblsmAir.AutoSize = true;
            this.lblsmAir.Location = new System.Drawing.Point(169, 298);
            this.lblsmAir.Name = "lblsmAir";
            this.lblsmAir.Size = new System.Drawing.Size(15, 19);
            this.lblsmAir.TabIndex = 3;
            this.lblsmAir.Text = "-";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(38, 228);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(80, 19);
            this.metroLabel7.TabIndex = 2;
            this.metroLabel7.Text = "Power Index";
            // 
            // lblsmAltd
            // 
            this.lblsmAltd.AutoSize = true;
            this.lblsmAltd.Location = new System.Drawing.Point(169, 157);
            this.lblsmAltd.Name = "lblsmAltd";
            this.lblsmAltd.Size = new System.Drawing.Size(15, 19);
            this.lblsmAltd.TabIndex = 3;
            this.lblsmAltd.Text = "-";
            // 
            // lblsmUS
            // 
            this.lblsmUS.AutoSize = true;
            this.lblsmUS.Location = new System.Drawing.Point(169, 334);
            this.lblsmUS.Name = "lblsmUS";
            this.lblsmUS.Size = new System.Drawing.Size(15, 19);
            this.lblsmUS.TabIndex = 3;
            this.lblsmUS.Text = "-";
            // 
            // lblsmPower
            // 
            this.lblsmPower.AutoSize = true;
            this.lblsmPower.Location = new System.Drawing.Point(169, 192);
            this.lblsmPower.Name = "lblsmPower";
            this.lblsmPower.Size = new System.Drawing.Size(15, 19);
            this.lblsmPower.TabIndex = 3;
            this.lblsmPower.Text = "-";
            // 
            // lblsmHRCC
            // 
            this.lblsmHRCC.AutoSize = true;
            this.lblsmHRCC.Location = new System.Drawing.Point(169, 369);
            this.lblsmHRCC.Name = "lblsmHRCC";
            this.lblsmHRCC.Size = new System.Drawing.Size(15, 19);
            this.lblsmHRCC.TabIndex = 3;
            this.lblsmHRCC.Text = "-";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(58, 121);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(60, 19);
            this.metroLabel4.TabIndex = 2;
            this.metroLabel4.Text = "Cadence";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(72, 85);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(46, 19);
            this.metroLabel3.TabIndex = 2;
            this.metroLabel3.Text = "Speed";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(36, 335);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(82, 19);
            this.metroLabel10.TabIndex = 2;
            this.metroLabel10.Text = "US/Euro unit";
            // 
            // metroLabel19
            // 
            this.metroLabel19.AutoSize = true;
            this.metroLabel19.Location = new System.Drawing.Point(70, 369);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(48, 19);
            this.metroLabel19.TabIndex = 2;
            this.metroLabel19.Text = "HR/CC";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(99, 11);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(55, 19);
            this.metroLabel2.TabIndex = 2;
            this.metroLabel2.Text = "SMODE";
            // 
            // metroPanel2
            // 
            this.metroPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.metroPanel2.Controls.Add(this.metroLabel2);
            this.metroPanel2.Controls.Add(this.metroLabel19);
            this.metroPanel2.Controls.Add(this.metroLabel10);
            this.metroPanel2.Controls.Add(this.metroLabel3);
            this.metroPanel2.Controls.Add(this.metroLabel4);
            this.metroPanel2.Controls.Add(this.lblsmHRCC);
            this.metroPanel2.Controls.Add(this.lblsmPower);
            this.metroPanel2.Controls.Add(this.lblsmUS);
            this.metroPanel2.Controls.Add(this.lblsmAltd);
            this.metroPanel2.Controls.Add(this.metroLabel7);
            this.metroPanel2.Controls.Add(this.lblsmAir);
            this.metroPanel2.Controls.Add(this.metroLabel8);
            this.metroPanel2.Controls.Add(this.lblsmCad);
            this.metroPanel2.Controls.Add(this.metroLabel5);
            this.metroPanel2.Controls.Add(this.lblsmPowerIndex);
            this.metroPanel2.Controls.Add(this.lblsmPowBal);
            this.metroPanel2.Controls.Add(this.metroLabel9);
            this.metroPanel2.Controls.Add(this.lblsmSpeed);
            this.metroPanel2.Controls.Add(this.metroLabel6);
            this.metroPanel2.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(1040, 166);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(241, 422);
            this.metroPanel2.TabIndex = 2;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // lblsmPowerIndex
            // 
            this.lblsmPowerIndex.AutoSize = true;
            this.lblsmPowerIndex.Location = new System.Drawing.Point(169, 228);
            this.lblsmPowerIndex.Name = "lblsmPowerIndex";
            this.lblsmPowerIndex.Size = new System.Drawing.Size(15, 19);
            this.lblsmPowerIndex.TabIndex = 3;
            this.lblsmPowerIndex.Text = "-";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TimeInterval,
            this.HeartRate,
            this.Speeds,
            this.cadences,
            this.Altitudes,
            this.PowerS,
            this.powerBalance,
            this.PIS,
            this.lrds});
            this.dataGridView1.Location = new System.Drawing.Point(324, 166);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(643, 421);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseUp);
            // 
            // TimeInterval
            // 
            this.TimeInterval.HeaderText = "TimeInterval";
            this.TimeInterval.Name = "TimeInterval";
            // 
            // HeartRate
            // 
            this.HeartRate.HeaderText = "HeartRate";
            this.HeartRate.Name = "HeartRate";
            // 
            // Speeds
            // 
            this.Speeds.HeaderText = "Speed";
            this.Speeds.Name = "Speeds";
            // 
            // cadences
            // 
            this.cadences.HeaderText = "Cadence";
            this.cadences.Name = "cadences";
            // 
            // Altitudes
            // 
            this.Altitudes.HeaderText = "Altitude";
            this.Altitudes.Name = "Altitudes";
            // 
            // PowerS
            // 
            this.PowerS.HeaderText = "Power_Watt";
            this.PowerS.Name = "PowerS";
            // 
            // powerBalance
            // 
            this.powerBalance.HeaderText = "powerBalance";
            this.powerBalance.Name = "powerBalance";
            // 
            // PIS
            // 
            this.PIS.HeaderText = "Pedalling Index";
            this.PIS.Name = "PIS";
            // 
            // lrds
            // 
            this.lrds.HeaderText = "Left Right balance";
            this.lrds.Name = "lrds";
            // 
            // btnadvancedMetrx
            // 
            this.btnadvancedMetrx.AutoSize = true;
            this.btnadvancedMetrx.Location = new System.Drawing.Point(497, 91);
            this.btnadvancedMetrx.Name = "btnadvancedMetrx";
            this.btnadvancedMetrx.Size = new System.Drawing.Size(143, 24);
            this.btnadvancedMetrx.TabIndex = 3;
            this.btnadvancedMetrx.Text = "Advanced Metrix";
            this.btnadvancedMetrx.UseSelectable = true;
            this.btnadvancedMetrx.Click += new System.EventHandler(this.btnadvancedMetrx_Click);
            // 
            // btnIntervalDetection
            // 
            this.btnIntervalDetection.AutoSize = true;
            this.btnIntervalDetection.Location = new System.Drawing.Point(646, 91);
            this.btnIntervalDetection.Name = "btnIntervalDetection";
            this.btnIntervalDetection.Size = new System.Drawing.Size(155, 24);
            this.btnIntervalDetection.TabIndex = 3;
            this.btnIntervalDetection.Text = "Interval Detection";
            this.btnIntervalDetection.UseSelectable = true;
            this.btnIntervalDetection.Click += new System.EventHandler(this.btnIntervalDetection_Click);
            // 
            // btnFileComparison
            // 
            this.btnFileComparison.AutoSize = true;
            this.btnFileComparison.Location = new System.Drawing.Point(812, 91);
            this.btnFileComparison.Name = "btnFileComparison";
            this.btnFileComparison.Size = new System.Drawing.Size(155, 24);
            this.btnFileComparison.TabIndex = 3;
            this.btnFileComparison.Text = "File Comparison";
            this.btnFileComparison.UseSelectable = true;
            this.btnFileComparison.Click += new System.EventHandler(this.btnFileComparison_Click);
            // 
            // btnCalendar
            // 
            this.btnCalendar.AutoSize = true;
            this.btnCalendar.Location = new System.Drawing.Point(975, 91);
            this.btnCalendar.Name = "btnCalendar";
            this.btnCalendar.Size = new System.Drawing.Size(155, 24);
            this.btnCalendar.TabIndex = 3;
            this.btnCalendar.Text = "Calendar Feature ";
            this.btnCalendar.UseSelectable = true;
            this.btnCalendar.Click += new System.EventHandler(this.btnCalendar_Click);
            // 
            // OpenFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1324, 677);
            this.Controls.Add(this.btnViewSummary);
            this.Controls.Add(this.btnViewGraph);
            this.Controls.Add(this.btnCalendar);
            this.Controls.Add(this.btnFileComparison);
            this.Controls.Add(this.btnIntervalDetection);
            this.Controls.Add(this.btnadvancedMetrx);
            this.Controls.Add(this.btnLoadHRM);
            this.Controls.Add(this.metroPanel2);
            this.Controls.Add(this.panelDetails);
            this.Controls.Add(this.dataGridView1);
            this.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.Name = "OpenFile";
            this.Text = "Dashboard";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panelDetails.ResumeLayout(false);
            this.panelDetails.PerformLayout();
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroPanel panelDetails;
        private MetroFramework.Controls.MetroButton btnLoadHRM;
        private MetroFramework.Controls.MetroButton btnViewGraph;
        private MetroFramework.Controls.MetroButton btnViewSummary;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel lblInterval;
        private MetroFramework.Controls.MetroLabel lblWeight;
        private MetroFramework.Controls.MetroLabel lblSmode;
        private MetroFramework.Controls.MetroLabel lblStartTime;
        private MetroFramework.Controls.MetroLabel lblDate;
        private MetroFramework.Controls.MetroLabel lblLength;
        private MetroFramework.Controls.MetroLabel lblMonitor;
        private MetroFramework.Controls.MetroLabel lblVersion;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private MetroFramework.Controls.MetroLabel metroLabel17;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel lblsmSpeed;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel lblsmPowBal;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel lblsmCad;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel lblsmAir;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel lblsmAltd;
        private MetroFramework.Controls.MetroLabel lblsmUS;
        private MetroFramework.Controls.MetroLabel lblsmPower;
        private MetroFramework.Controls.MetroLabel lblsmHRCC;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel19;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroLabel lblsmPowerIndex;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimeInterval;
        private System.Windows.Forms.DataGridViewTextBoxColumn HeartRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Speeds;
        private System.Windows.Forms.DataGridViewTextBoxColumn cadences;
        private System.Windows.Forms.DataGridViewTextBoxColumn Altitudes;
        private System.Windows.Forms.DataGridViewTextBoxColumn PowerS;
        private System.Windows.Forms.DataGridViewTextBoxColumn powerBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn PIS;
        private System.Windows.Forms.DataGridViewTextBoxColumn lrds;
        private MetroFramework.Controls.MetroButton btnadvancedMetrx;
        private MetroFramework.Controls.MetroButton btnIntervalDetection;
        private MetroFramework.Controls.MetroButton btnFileComparison;
        private MetroFramework.Controls.MetroButton btnCalendar;
    }
}

