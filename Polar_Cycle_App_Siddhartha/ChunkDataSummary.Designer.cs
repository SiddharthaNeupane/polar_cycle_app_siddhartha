﻿namespace Polar_Cycle_App_Siddhartha
{
    partial class ChunkDataSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Chunk1 = new System.Windows.Forms.TabPage();
            this.Chunk2 = new System.Windows.Forms.TabPage();
            this.Chunk3 = new System.Windows.Forms.TabPage();
            this.Chunk4 = new System.Windows.Forms.TabPage();
            this.maxa1 = new System.Windows.Forms.Label();
            this.aa1 = new System.Windows.Forms.Label();
            this.maxp1 = new System.Windows.Forms.Label();
            this.ap1 = new System.Windows.Forms.Label();
            this.minHR1 = new System.Windows.Forms.Label();
            this.maxHeartRate1 = new System.Windows.Forms.Label();
            this.avgHeartRate1 = new System.Windows.Forms.Label();
            this.maxsp1 = new System.Windows.Forms.Label();
            this.averagesp1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.maxa2 = new System.Windows.Forms.Label();
            this.aa2 = new System.Windows.Forms.Label();
            this.maxp2 = new System.Windows.Forms.Label();
            this.ap2 = new System.Windows.Forms.Label();
            this.maxHR2 = new System.Windows.Forms.Label();
            this.minHR2 = new System.Windows.Forms.Label();
            this.averagehrate2 = new System.Windows.Forms.Label();
            this.maxsp2 = new System.Windows.Forms.Label();
            this.averagesp2 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.maxa3 = new System.Windows.Forms.Label();
            this.aa3 = new System.Windows.Forms.Label();
            this.maxp3 = new System.Windows.Forms.Label();
            this.ap3 = new System.Windows.Forms.Label();
            this.minhr3 = new System.Windows.Forms.Label();
            this.maxhr3 = new System.Windows.Forms.Label();
            this.averagehr3 = new System.Windows.Forms.Label();
            this.maxsp3 = new System.Windows.Forms.Label();
            this.averagesp3 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.maxa4 = new System.Windows.Forms.Label();
            this.aa4 = new System.Windows.Forms.Label();
            this.maxp4 = new System.Windows.Forms.Label();
            this.ap4 = new System.Windows.Forms.Label();
            this.minhr4 = new System.Windows.Forms.Label();
            this.maxhr4 = new System.Windows.Forms.Label();
            this.averagehr4 = new System.Windows.Forms.Label();
            this.maxsp4 = new System.Windows.Forms.Label();
            this.averagesp4 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.Chunk1.SuspendLayout();
            this.Chunk2.SuspendLayout();
            this.Chunk3.SuspendLayout();
            this.Chunk4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Chunk1);
            this.tabControl1.Controls.Add(this.Chunk2);
            this.tabControl1.Controls.Add(this.Chunk3);
            this.tabControl1.Controls.Add(this.Chunk4);
            this.tabControl1.Location = new System.Drawing.Point(26, 95);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(714, 451);
            this.tabControl1.TabIndex = 0;
            // 
            // Chunk1
            // 
            this.Chunk1.Controls.Add(this.maxa1);
            this.Chunk1.Controls.Add(this.aa1);
            this.Chunk1.Controls.Add(this.maxp1);
            this.Chunk1.Controls.Add(this.ap1);
            this.Chunk1.Controls.Add(this.minHR1);
            this.Chunk1.Controls.Add(this.maxHeartRate1);
            this.Chunk1.Controls.Add(this.avgHeartRate1);
            this.Chunk1.Controls.Add(this.maxsp1);
            this.Chunk1.Controls.Add(this.averagesp1);
            this.Chunk1.Controls.Add(this.label11);
            this.Chunk1.Controls.Add(this.label10);
            this.Chunk1.Controls.Add(this.label9);
            this.Chunk1.Controls.Add(this.label8);
            this.Chunk1.Controls.Add(this.label7);
            this.Chunk1.Controls.Add(this.label6);
            this.Chunk1.Controls.Add(this.label5);
            this.Chunk1.Controls.Add(this.label4);
            this.Chunk1.Controls.Add(this.label3);
            this.Chunk1.Location = new System.Drawing.Point(4, 22);
            this.Chunk1.Name = "Chunk1";
            this.Chunk1.Padding = new System.Windows.Forms.Padding(3);
            this.Chunk1.Size = new System.Drawing.Size(706, 425);
            this.Chunk1.TabIndex = 0;
            this.Chunk1.Text = "Chunk1";
            this.Chunk1.UseVisualStyleBackColor = true;
            // 
            // Chunk2
            // 
            this.Chunk2.Controls.Add(this.maxa2);
            this.Chunk2.Controls.Add(this.aa2);
            this.Chunk2.Controls.Add(this.maxp2);
            this.Chunk2.Controls.Add(this.ap2);
            this.Chunk2.Controls.Add(this.maxHR2);
            this.Chunk2.Controls.Add(this.minHR2);
            this.Chunk2.Controls.Add(this.averagehrate2);
            this.Chunk2.Controls.Add(this.maxsp2);
            this.Chunk2.Controls.Add(this.averagesp2);
            this.Chunk2.Controls.Add(this.label32);
            this.Chunk2.Controls.Add(this.label33);
            this.Chunk2.Controls.Add(this.label34);
            this.Chunk2.Controls.Add(this.label35);
            this.Chunk2.Controls.Add(this.label36);
            this.Chunk2.Controls.Add(this.label37);
            this.Chunk2.Controls.Add(this.label38);
            this.Chunk2.Controls.Add(this.label39);
            this.Chunk2.Controls.Add(this.label40);
            this.Chunk2.Location = new System.Drawing.Point(4, 22);
            this.Chunk2.Name = "Chunk2";
            this.Chunk2.Padding = new System.Windows.Forms.Padding(3);
            this.Chunk2.Size = new System.Drawing.Size(706, 425);
            this.Chunk2.TabIndex = 1;
            this.Chunk2.Text = "Chunk2";
            this.Chunk2.UseVisualStyleBackColor = true;
            // 
            // Chunk3
            // 
            this.Chunk3.Controls.Add(this.maxa3);
            this.Chunk3.Controls.Add(this.aa3);
            this.Chunk3.Controls.Add(this.maxp3);
            this.Chunk3.Controls.Add(this.ap3);
            this.Chunk3.Controls.Add(this.minhr3);
            this.Chunk3.Controls.Add(this.maxhr3);
            this.Chunk3.Controls.Add(this.averagehr3);
            this.Chunk3.Controls.Add(this.maxsp3);
            this.Chunk3.Controls.Add(this.averagesp3);
            this.Chunk3.Controls.Add(this.label52);
            this.Chunk3.Controls.Add(this.label53);
            this.Chunk3.Controls.Add(this.label54);
            this.Chunk3.Controls.Add(this.label55);
            this.Chunk3.Controls.Add(this.label56);
            this.Chunk3.Controls.Add(this.label57);
            this.Chunk3.Controls.Add(this.label58);
            this.Chunk3.Controls.Add(this.label59);
            this.Chunk3.Controls.Add(this.label60);
            this.Chunk3.Location = new System.Drawing.Point(4, 22);
            this.Chunk3.Name = "Chunk3";
            this.Chunk3.Padding = new System.Windows.Forms.Padding(3);
            this.Chunk3.Size = new System.Drawing.Size(706, 425);
            this.Chunk3.TabIndex = 2;
            this.Chunk3.Text = "Chunk3";
            this.Chunk3.UseVisualStyleBackColor = true;
            // 
            // Chunk4
            // 
            this.Chunk4.Controls.Add(this.maxa4);
            this.Chunk4.Controls.Add(this.aa4);
            this.Chunk4.Controls.Add(this.maxp4);
            this.Chunk4.Controls.Add(this.ap4);
            this.Chunk4.Controls.Add(this.minhr4);
            this.Chunk4.Controls.Add(this.maxhr4);
            this.Chunk4.Controls.Add(this.averagehr4);
            this.Chunk4.Controls.Add(this.maxsp4);
            this.Chunk4.Controls.Add(this.averagesp4);
            this.Chunk4.Controls.Add(this.label72);
            this.Chunk4.Controls.Add(this.label73);
            this.Chunk4.Controls.Add(this.label74);
            this.Chunk4.Controls.Add(this.label75);
            this.Chunk4.Controls.Add(this.label76);
            this.Chunk4.Controls.Add(this.label77);
            this.Chunk4.Controls.Add(this.label78);
            this.Chunk4.Controls.Add(this.label79);
            this.Chunk4.Controls.Add(this.label80);
            this.Chunk4.Location = new System.Drawing.Point(4, 22);
            this.Chunk4.Name = "Chunk4";
            this.Chunk4.Padding = new System.Windows.Forms.Padding(3);
            this.Chunk4.Size = new System.Drawing.Size(706, 425);
            this.Chunk4.TabIndex = 3;
            this.Chunk4.Text = "Chunk4";
            this.Chunk4.UseVisualStyleBackColor = true;
            // 
            // maxa1
            // 
            this.maxa1.AutoSize = true;
            this.maxa1.Location = new System.Drawing.Point(560, 336);
            this.maxa1.Name = "maxa1";
            this.maxa1.Size = new System.Drawing.Size(28, 13);
            this.maxa1.TabIndex = 39;
            this.maxa1.Text = "0.00";
            this.maxa1.UseWaitCursor = true;
            // 
            // aa1
            // 
            this.aa1.AutoSize = true;
            this.aa1.Location = new System.Drawing.Point(560, 264);
            this.aa1.Name = "aa1";
            this.aa1.Size = new System.Drawing.Size(28, 13);
            this.aa1.TabIndex = 38;
            this.aa1.Text = "0.00";
            this.aa1.UseWaitCursor = true;
            // 
            // maxp1
            // 
            this.maxp1.AutoSize = true;
            this.maxp1.Location = new System.Drawing.Point(560, 204);
            this.maxp1.Name = "maxp1";
            this.maxp1.Size = new System.Drawing.Size(28, 13);
            this.maxp1.TabIndex = 37;
            this.maxp1.Text = "0.00";
            this.maxp1.UseWaitCursor = true;
            // 
            // ap1
            // 
            this.ap1.AutoSize = true;
            this.ap1.Location = new System.Drawing.Point(560, 147);
            this.ap1.Name = "ap1";
            this.ap1.Size = new System.Drawing.Size(28, 13);
            this.ap1.TabIndex = 36;
            this.ap1.Text = "0.00";
            this.ap1.UseWaitCursor = true;
            // 
            // minHR1
            // 
            this.minHR1.AutoSize = true;
            this.minHR1.Location = new System.Drawing.Point(560, 90);
            this.minHR1.Name = "minHR1";
            this.minHR1.Size = new System.Drawing.Size(28, 13);
            this.minHR1.TabIndex = 35;
            this.minHR1.Text = "0.00";
            this.minHR1.UseWaitCursor = true;
            // 
            // maxHeartRate1
            // 
            this.maxHeartRate1.AutoSize = true;
            this.maxHeartRate1.Location = new System.Drawing.Point(235, 315);
            this.maxHeartRate1.Name = "maxHeartRate1";
            this.maxHeartRate1.Size = new System.Drawing.Size(28, 13);
            this.maxHeartRate1.TabIndex = 34;
            this.maxHeartRate1.Text = "0.00";
            this.maxHeartRate1.UseWaitCursor = true;
            // 
            // avgHeartRate1
            // 
            this.avgHeartRate1.AutoSize = true;
            this.avgHeartRate1.Location = new System.Drawing.Point(235, 243);
            this.avgHeartRate1.Name = "avgHeartRate1";
            this.avgHeartRate1.Size = new System.Drawing.Size(28, 13);
            this.avgHeartRate1.TabIndex = 33;
            this.avgHeartRate1.Text = "0.00";
            this.avgHeartRate1.UseWaitCursor = true;
            // 
            // maxsp1
            // 
            this.maxsp1.AutoSize = true;
            this.maxsp1.Location = new System.Drawing.Point(235, 183);
            this.maxsp1.Name = "maxsp1";
            this.maxsp1.Size = new System.Drawing.Size(28, 13);
            this.maxsp1.TabIndex = 32;
            this.maxsp1.Text = "0.00";
            this.maxsp1.UseWaitCursor = true;
            // 
            // averagesp1
            // 
            this.averagesp1.AutoSize = true;
            this.averagesp1.Location = new System.Drawing.Point(235, 126);
            this.averagesp1.Name = "averagesp1";
            this.averagesp1.Size = new System.Drawing.Size(28, 13);
            this.averagesp1.TabIndex = 31;
            this.averagesp1.Text = "0.00";
            this.averagesp1.UseWaitCursor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(417, 336);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "Maximum Altitude";
            this.label11.UseWaitCursor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(417, 264);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "Average Ailtitude";
            this.label10.UseWaitCursor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(417, 204);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "Maximum Power";
            this.label9.UseWaitCursor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(417, 147);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "Average Power";
            this.label8.UseWaitCursor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(417, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Minimum Heart Rate";
            this.label7.UseWaitCursor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(82, 315);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Maximum Heart Rate";
            this.label6.UseWaitCursor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(82, 243);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Average Heart Rate";
            this.label5.UseWaitCursor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(82, 183);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Maximum Speed";
            this.label4.UseWaitCursor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(82, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Average Speed";
            this.label3.UseWaitCursor = true;
            // 
            // maxa2
            // 
            this.maxa2.AutoSize = true;
            this.maxa2.Location = new System.Drawing.Point(559, 330);
            this.maxa2.Name = "maxa2";
            this.maxa2.Size = new System.Drawing.Size(28, 13);
            this.maxa2.TabIndex = 59;
            this.maxa2.Text = "0.00";
            this.maxa2.UseWaitCursor = true;
            // 
            // aa2
            // 
            this.aa2.AutoSize = true;
            this.aa2.Location = new System.Drawing.Point(559, 258);
            this.aa2.Name = "aa2";
            this.aa2.Size = new System.Drawing.Size(28, 13);
            this.aa2.TabIndex = 58;
            this.aa2.Text = "0.00";
            this.aa2.UseWaitCursor = true;
            // 
            // maxp2
            // 
            this.maxp2.AutoSize = true;
            this.maxp2.Location = new System.Drawing.Point(559, 198);
            this.maxp2.Name = "maxp2";
            this.maxp2.Size = new System.Drawing.Size(28, 13);
            this.maxp2.TabIndex = 57;
            this.maxp2.Text = "0.00";
            this.maxp2.UseWaitCursor = true;
            // 
            // ap2
            // 
            this.ap2.AutoSize = true;
            this.ap2.Location = new System.Drawing.Point(559, 141);
            this.ap2.Name = "ap2";
            this.ap2.Size = new System.Drawing.Size(28, 13);
            this.ap2.TabIndex = 56;
            this.ap2.Text = "0.00";
            this.ap2.UseWaitCursor = true;
            // 
            // maxHR2
            // 
            this.maxHR2.AutoSize = true;
            this.maxHR2.Location = new System.Drawing.Point(559, 84);
            this.maxHR2.Name = "maxHR2";
            this.maxHR2.Size = new System.Drawing.Size(28, 13);
            this.maxHR2.TabIndex = 55;
            this.maxHR2.Text = "0.00";
            this.maxHR2.UseWaitCursor = true;
            // 
            // minHR2
            // 
            this.minHR2.AutoSize = true;
            this.minHR2.Location = new System.Drawing.Point(239, 308);
            this.minHR2.Name = "minHR2";
            this.minHR2.Size = new System.Drawing.Size(28, 13);
            this.minHR2.TabIndex = 54;
            this.minHR2.Text = "0.00";
            this.minHR2.UseWaitCursor = true;
            // 
            // averagehrate2
            // 
            this.averagehrate2.AutoSize = true;
            this.averagehrate2.Location = new System.Drawing.Point(239, 236);
            this.averagehrate2.Name = "averagehrate2";
            this.averagehrate2.Size = new System.Drawing.Size(28, 13);
            this.averagehrate2.TabIndex = 53;
            this.averagehrate2.Text = "0.00";
            this.averagehrate2.UseWaitCursor = true;
            // 
            // maxsp2
            // 
            this.maxsp2.AutoSize = true;
            this.maxsp2.Location = new System.Drawing.Point(239, 176);
            this.maxsp2.Name = "maxsp2";
            this.maxsp2.Size = new System.Drawing.Size(28, 13);
            this.maxsp2.TabIndex = 52;
            this.maxsp2.Text = "0.00";
            this.maxsp2.UseWaitCursor = true;
            // 
            // averagesp2
            // 
            this.averagesp2.AutoSize = true;
            this.averagesp2.Location = new System.Drawing.Point(239, 119);
            this.averagesp2.Name = "averagesp2";
            this.averagesp2.Size = new System.Drawing.Size(28, 13);
            this.averagesp2.TabIndex = 51;
            this.averagesp2.Text = "0.00";
            this.averagesp2.UseWaitCursor = true;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(416, 330);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(89, 13);
            this.label32.TabIndex = 49;
            this.label32.Text = "Maximum Altitude";
            this.label32.UseWaitCursor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(416, 258);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(87, 13);
            this.label33.TabIndex = 48;
            this.label33.Text = "Average Ailtitude";
            this.label33.UseWaitCursor = true;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(416, 198);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(84, 13);
            this.label34.TabIndex = 47;
            this.label34.Text = "Maximum Power";
            this.label34.UseWaitCursor = true;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(416, 141);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(80, 13);
            this.label35.TabIndex = 46;
            this.label35.Text = "Average Power";
            this.label35.UseWaitCursor = true;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(416, 84);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(103, 13);
            this.label36.TabIndex = 45;
            this.label36.Text = "Minimum Heart Rate";
            this.label36.UseWaitCursor = true;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(86, 308);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(106, 13);
            this.label37.TabIndex = 44;
            this.label37.Text = "Maximum Heart Rate";
            this.label37.UseWaitCursor = true;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(86, 236);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(102, 13);
            this.label38.TabIndex = 43;
            this.label38.Text = "Average Heart Rate";
            this.label38.UseWaitCursor = true;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(86, 176);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(85, 13);
            this.label39.TabIndex = 42;
            this.label39.Text = "Maximum Speed";
            this.label39.UseWaitCursor = true;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(86, 119);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(81, 13);
            this.label40.TabIndex = 41;
            this.label40.Text = "Average Speed";
            this.label40.UseWaitCursor = true;
            // 
            // maxa3
            // 
            this.maxa3.AutoSize = true;
            this.maxa3.Location = new System.Drawing.Point(557, 331);
            this.maxa3.Name = "maxa3";
            this.maxa3.Size = new System.Drawing.Size(28, 13);
            this.maxa3.TabIndex = 59;
            this.maxa3.Text = "0.00";
            this.maxa3.UseWaitCursor = true;
            // 
            // aa3
            // 
            this.aa3.AutoSize = true;
            this.aa3.Location = new System.Drawing.Point(557, 259);
            this.aa3.Name = "aa3";
            this.aa3.Size = new System.Drawing.Size(28, 13);
            this.aa3.TabIndex = 58;
            this.aa3.Text = "0.00";
            this.aa3.UseWaitCursor = true;
            // 
            // maxp3
            // 
            this.maxp3.AutoSize = true;
            this.maxp3.Location = new System.Drawing.Point(557, 199);
            this.maxp3.Name = "maxp3";
            this.maxp3.Size = new System.Drawing.Size(28, 13);
            this.maxp3.TabIndex = 57;
            this.maxp3.Text = "0.00";
            this.maxp3.UseWaitCursor = true;
            // 
            // ap3
            // 
            this.ap3.AutoSize = true;
            this.ap3.Location = new System.Drawing.Point(557, 142);
            this.ap3.Name = "ap3";
            this.ap3.Size = new System.Drawing.Size(28, 13);
            this.ap3.TabIndex = 56;
            this.ap3.Text = "0.00";
            this.ap3.UseWaitCursor = true;
            // 
            // minhr3
            // 
            this.minhr3.AutoSize = true;
            this.minhr3.Location = new System.Drawing.Point(557, 85);
            this.minhr3.Name = "minhr3";
            this.minhr3.Size = new System.Drawing.Size(28, 13);
            this.minhr3.TabIndex = 55;
            this.minhr3.Text = "0.00";
            this.minhr3.UseWaitCursor = true;
            // 
            // maxhr3
            // 
            this.maxhr3.AutoSize = true;
            this.maxhr3.Location = new System.Drawing.Point(239, 301);
            this.maxhr3.Name = "maxhr3";
            this.maxhr3.Size = new System.Drawing.Size(28, 13);
            this.maxhr3.TabIndex = 54;
            this.maxhr3.Text = "0.00";
            this.maxhr3.UseWaitCursor = true;
            // 
            // averagehr3
            // 
            this.averagehr3.AutoSize = true;
            this.averagehr3.Location = new System.Drawing.Point(239, 229);
            this.averagehr3.Name = "averagehr3";
            this.averagehr3.Size = new System.Drawing.Size(28, 13);
            this.averagehr3.TabIndex = 53;
            this.averagehr3.Text = "0.00";
            this.averagehr3.UseWaitCursor = true;
            // 
            // maxsp3
            // 
            this.maxsp3.AutoSize = true;
            this.maxsp3.Location = new System.Drawing.Point(239, 169);
            this.maxsp3.Name = "maxsp3";
            this.maxsp3.Size = new System.Drawing.Size(28, 13);
            this.maxsp3.TabIndex = 52;
            this.maxsp3.Text = "0.00";
            this.maxsp3.UseWaitCursor = true;
            // 
            // averagesp3
            // 
            this.averagesp3.AutoSize = true;
            this.averagesp3.Location = new System.Drawing.Point(239, 112);
            this.averagesp3.Name = "averagesp3";
            this.averagesp3.Size = new System.Drawing.Size(28, 13);
            this.averagesp3.TabIndex = 51;
            this.averagesp3.Text = "0.00";
            this.averagesp3.UseWaitCursor = true;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(414, 331);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(89, 13);
            this.label52.TabIndex = 49;
            this.label52.Text = "Maximum Altitude";
            this.label52.UseWaitCursor = true;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(414, 259);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(87, 13);
            this.label53.TabIndex = 48;
            this.label53.Text = "Average Ailtitude";
            this.label53.UseWaitCursor = true;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(414, 199);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(84, 13);
            this.label54.TabIndex = 47;
            this.label54.Text = "Maximum Power";
            this.label54.UseWaitCursor = true;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(414, 142);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(80, 13);
            this.label55.TabIndex = 46;
            this.label55.Text = "Average Power";
            this.label55.UseWaitCursor = true;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(414, 85);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(103, 13);
            this.label56.TabIndex = 45;
            this.label56.Text = "Minimum Heart Rate";
            this.label56.UseWaitCursor = true;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(86, 301);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(106, 13);
            this.label57.TabIndex = 44;
            this.label57.Text = "Maximum Heart Rate";
            this.label57.UseWaitCursor = true;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(86, 229);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(102, 13);
            this.label58.TabIndex = 43;
            this.label58.Text = "Average Heart Rate";
            this.label58.UseWaitCursor = true;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(86, 169);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(85, 13);
            this.label59.TabIndex = 42;
            this.label59.Text = "Maximum Speed";
            this.label59.UseWaitCursor = true;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(86, 112);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(81, 13);
            this.label60.TabIndex = 41;
            this.label60.Text = "Average Speed";
            this.label60.UseWaitCursor = true;
            // 
            // maxa4
            // 
            this.maxa4.AutoSize = true;
            this.maxa4.Location = new System.Drawing.Point(560, 327);
            this.maxa4.Name = "maxa4";
            this.maxa4.Size = new System.Drawing.Size(28, 13);
            this.maxa4.TabIndex = 59;
            this.maxa4.Text = "0.00";
            this.maxa4.UseWaitCursor = true;
            // 
            // aa4
            // 
            this.aa4.AutoSize = true;
            this.aa4.Location = new System.Drawing.Point(560, 255);
            this.aa4.Name = "aa4";
            this.aa4.Size = new System.Drawing.Size(28, 13);
            this.aa4.TabIndex = 58;
            this.aa4.Text = "0.00";
            this.aa4.UseWaitCursor = true;
            // 
            // maxp4
            // 
            this.maxp4.AutoSize = true;
            this.maxp4.Location = new System.Drawing.Point(560, 195);
            this.maxp4.Name = "maxp4";
            this.maxp4.Size = new System.Drawing.Size(28, 13);
            this.maxp4.TabIndex = 57;
            this.maxp4.Text = "0.00";
            this.maxp4.UseWaitCursor = true;
            // 
            // ap4
            // 
            this.ap4.AutoSize = true;
            this.ap4.Location = new System.Drawing.Point(560, 138);
            this.ap4.Name = "ap4";
            this.ap4.Size = new System.Drawing.Size(28, 13);
            this.ap4.TabIndex = 56;
            this.ap4.Text = "0.00";
            this.ap4.UseWaitCursor = true;
            // 
            // minhr4
            // 
            this.minhr4.AutoSize = true;
            this.minhr4.Location = new System.Drawing.Point(560, 81);
            this.minhr4.Name = "minhr4";
            this.minhr4.Size = new System.Drawing.Size(28, 13);
            this.minhr4.TabIndex = 55;
            this.minhr4.Text = "0.00";
            this.minhr4.UseWaitCursor = true;
            // 
            // maxhr4
            // 
            this.maxhr4.AutoSize = true;
            this.maxhr4.Location = new System.Drawing.Point(242, 303);
            this.maxhr4.Name = "maxhr4";
            this.maxhr4.Size = new System.Drawing.Size(28, 13);
            this.maxhr4.TabIndex = 54;
            this.maxhr4.Text = "0.00";
            this.maxhr4.UseWaitCursor = true;
            // 
            // averagehr4
            // 
            this.averagehr4.AutoSize = true;
            this.averagehr4.Location = new System.Drawing.Point(242, 231);
            this.averagehr4.Name = "averagehr4";
            this.averagehr4.Size = new System.Drawing.Size(28, 13);
            this.averagehr4.TabIndex = 53;
            this.averagehr4.Text = "0.00";
            this.averagehr4.UseWaitCursor = true;
            // 
            // maxsp4
            // 
            this.maxsp4.AutoSize = true;
            this.maxsp4.Location = new System.Drawing.Point(242, 171);
            this.maxsp4.Name = "maxsp4";
            this.maxsp4.Size = new System.Drawing.Size(28, 13);
            this.maxsp4.TabIndex = 52;
            this.maxsp4.Text = "0.00";
            this.maxsp4.UseWaitCursor = true;
            // 
            // averagesp4
            // 
            this.averagesp4.AutoSize = true;
            this.averagesp4.Location = new System.Drawing.Point(242, 114);
            this.averagesp4.Name = "averagesp4";
            this.averagesp4.Size = new System.Drawing.Size(28, 13);
            this.averagesp4.TabIndex = 51;
            this.averagesp4.Text = "0.00";
            this.averagesp4.UseWaitCursor = true;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(417, 327);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(89, 13);
            this.label72.TabIndex = 49;
            this.label72.Text = "Maximum Altitude";
            this.label72.UseWaitCursor = true;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(417, 255);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(87, 13);
            this.label73.TabIndex = 48;
            this.label73.Text = "Average Ailtitude";
            this.label73.UseWaitCursor = true;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(417, 195);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(84, 13);
            this.label74.TabIndex = 47;
            this.label74.Text = "Maximum Power";
            this.label74.UseWaitCursor = true;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(417, 138);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(80, 13);
            this.label75.TabIndex = 46;
            this.label75.Text = "Average Power";
            this.label75.UseWaitCursor = true;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(417, 81);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(103, 13);
            this.label76.TabIndex = 45;
            this.label76.Text = "Minimum Heart Rate";
            this.label76.UseWaitCursor = true;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(89, 303);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(106, 13);
            this.label77.TabIndex = 44;
            this.label77.Text = "Maximum Heart Rate";
            this.label77.UseWaitCursor = true;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(89, 231);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(102, 13);
            this.label78.TabIndex = 43;
            this.label78.Text = "Average Heart Rate";
            this.label78.UseWaitCursor = true;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(89, 171);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(85, 13);
            this.label79.TabIndex = 42;
            this.label79.Text = "Maximum Speed";
            this.label79.UseWaitCursor = true;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(89, 114);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(81, 13);
            this.label80.TabIndex = 41;
            this.label80.Text = "Average Speed";
            this.label80.UseWaitCursor = true;
            // 
            // ChunkDataSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 565);
            this.Controls.Add(this.tabControl1);
            this.Name = "ChunkDataSummary";
            this.Text = "ChunkDataSummary";
            this.Load += new System.EventHandler(this.ChunkDataSummary_Load);
            this.tabControl1.ResumeLayout(false);
            this.Chunk1.ResumeLayout(false);
            this.Chunk1.PerformLayout();
            this.Chunk2.ResumeLayout(false);
            this.Chunk2.PerformLayout();
            this.Chunk3.ResumeLayout(false);
            this.Chunk3.PerformLayout();
            this.Chunk4.ResumeLayout(false);
            this.Chunk4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Chunk1;
        private System.Windows.Forms.TabPage Chunk2;
        private System.Windows.Forms.TabPage Chunk3;
        private System.Windows.Forms.TabPage Chunk4;
        private System.Windows.Forms.Label maxa1;
        private System.Windows.Forms.Label aa1;
        private System.Windows.Forms.Label maxp1;
        private System.Windows.Forms.Label ap1;
        private System.Windows.Forms.Label minHR1;
        private System.Windows.Forms.Label maxHeartRate1;
        private System.Windows.Forms.Label avgHeartRate1;
        private System.Windows.Forms.Label maxsp1;
        private System.Windows.Forms.Label averagesp1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label maxa2;
        private System.Windows.Forms.Label aa2;
        private System.Windows.Forms.Label maxp2;
        private System.Windows.Forms.Label ap2;
        private System.Windows.Forms.Label maxHR2;
        private System.Windows.Forms.Label minHR2;
        private System.Windows.Forms.Label averagehrate2;
        private System.Windows.Forms.Label maxsp2;
        private System.Windows.Forms.Label averagesp2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label maxa3;
        private System.Windows.Forms.Label aa3;
        private System.Windows.Forms.Label maxp3;
        private System.Windows.Forms.Label ap3;
        private System.Windows.Forms.Label minhr3;
        private System.Windows.Forms.Label maxhr3;
        private System.Windows.Forms.Label averagehr3;
        private System.Windows.Forms.Label maxsp3;
        private System.Windows.Forms.Label averagesp3;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label maxa4;
        private System.Windows.Forms.Label aa4;
        private System.Windows.Forms.Label maxp4;
        private System.Windows.Forms.Label ap4;
        private System.Windows.Forms.Label minhr4;
        private System.Windows.Forms.Label maxhr4;
        private System.Windows.Forms.Label averagehr4;
        private System.Windows.Forms.Label maxsp4;
        private System.Windows.Forms.Label averagesp4;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
    }
}