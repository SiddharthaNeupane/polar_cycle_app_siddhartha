﻿namespace Polar_Cycle_App_Siddhartha
{
    partial class inputFiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.metroButton3 = new MetroFramework.Controls.MetroButton();
            this.fname1 = new MetroFramework.Controls.MetroLabel();
            this.fname2 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(156, 153);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(174, 39);
            this.metroButton1.TabIndex = 0;
            this.metroButton1.Text = "upload File one";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // metroButton2
            // 
            this.metroButton2.Location = new System.Drawing.Point(595, 153);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(174, 39);
            this.metroButton2.TabIndex = 0;
            this.metroButton2.Text = "Upload File two";
            this.metroButton2.UseSelectable = true;
            this.metroButton2.Click += new System.EventHandler(this.metroButton2_Click);
            // 
            // metroButton3
            // 
            this.metroButton3.Location = new System.Drawing.Point(357, 274);
            this.metroButton3.Name = "metroButton3";
            this.metroButton3.Size = new System.Drawing.Size(174, 39);
            this.metroButton3.TabIndex = 0;
            this.metroButton3.Text = "Compare";
            this.metroButton3.UseSelectable = true;
            this.metroButton3.Click += new System.EventHandler(this.metroButton3_Click);
            // 
            // fname1
            // 
            this.fname1.AutoSize = true;
            this.fname1.Location = new System.Drawing.Point(199, 222);
            this.fname1.Name = "fname1";
            this.fname1.Size = new System.Drawing.Size(58, 19);
            this.fname1.TabIndex = 1;
            this.fname1.Text = "File One";
            // 
            // fname2
            // 
            this.fname2.AutoSize = true;
            this.fname2.Location = new System.Drawing.Point(641, 222);
            this.fname2.Name = "fname2";
            this.fname2.Size = new System.Drawing.Size(54, 19);
            this.fname2.TabIndex = 1;
            this.fname2.Text = "File two";
            // 
            // inputFiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(910, 408);
            this.Controls.Add(this.fname2);
            this.Controls.Add(this.fname1);
            this.Controls.Add(this.metroButton3);
            this.Controls.Add(this.metroButton2);
            this.Controls.Add(this.metroButton1);
            this.Name = "inputFiles";
            this.Text = "Choose Files for Comparison";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroButton metroButton3;
        private MetroFramework.Controls.MetroLabel fname1;
        private MetroFramework.Controls.MetroLabel fname2;
    }
}