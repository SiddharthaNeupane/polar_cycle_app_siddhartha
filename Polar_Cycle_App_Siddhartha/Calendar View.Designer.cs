﻿namespace Polar_Cycle_App_Siddhartha
{
    partial class Calendar_View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOpenCalendar = new MetroFramework.Controls.MetroButton();
            this.CalendarData = new System.Windows.Forms.MonthCalendar();
            this.lstData = new System.Windows.Forms.ListBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.TimeInterval = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HeartRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Speeds = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cadences = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Altitudes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PowerS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.powerBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PIS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lrds = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOpenCalendar
            // 
            this.btnOpenCalendar.AutoSize = true;
            this.btnOpenCalendar.Location = new System.Drawing.Point(367, 82);
            this.btnOpenCalendar.Name = "btnOpenCalendar";
            this.btnOpenCalendar.Size = new System.Drawing.Size(155, 24);
            this.btnOpenCalendar.TabIndex = 4;
            this.btnOpenCalendar.Text = "Open Calendar Folder";
            this.btnOpenCalendar.UseSelectable = true;
            this.btnOpenCalendar.Click += new System.EventHandler(this.btnOpenCalendar_Click);
            // 
            // CalendarData
            // 
            this.CalendarData.Location = new System.Drawing.Point(53, 136);
            this.CalendarData.Name = "CalendarData";
            this.CalendarData.TabIndex = 5;
            this.CalendarData.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.CalendarData_DateSelected);
            // 
            // lstData
            // 
            this.lstData.FormattingEnabled = true;
            this.lstData.Location = new System.Drawing.Point(53, 337);
            this.lstData.Name = "lstData";
            this.lstData.Size = new System.Drawing.Size(241, 160);
            this.lstData.TabIndex = 6;
            this.lstData.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lstData_MouseClick);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TimeInterval,
            this.HeartRate,
            this.Speeds,
            this.cadences,
            this.Altitudes,
            this.PowerS,
            this.powerBalance,
            this.PIS,
            this.lrds});
            this.dataGridView1.Location = new System.Drawing.Point(314, 136);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(643, 421);
            this.dataGridView1.TabIndex = 7;
            // 
            // TimeInterval
            // 
            this.TimeInterval.HeaderText = "TimeInterval";
            this.TimeInterval.Name = "TimeInterval";
            // 
            // HeartRate
            // 
            this.HeartRate.HeaderText = "HeartRate";
            this.HeartRate.Name = "HeartRate";
            // 
            // Speeds
            // 
            this.Speeds.HeaderText = "Speed";
            this.Speeds.Name = "Speeds";
            // 
            // cadences
            // 
            this.cadences.HeaderText = "Cadence";
            this.cadences.Name = "cadences";
            // 
            // Altitudes
            // 
            this.Altitudes.HeaderText = "Altitude";
            this.Altitudes.Name = "Altitudes";
            // 
            // PowerS
            // 
            this.PowerS.HeaderText = "Power_Watt";
            this.PowerS.Name = "PowerS";
            // 
            // powerBalance
            // 
            this.powerBalance.HeaderText = "powerBalance";
            this.powerBalance.Name = "powerBalance";
            // 
            // PIS
            // 
            this.PIS.HeaderText = "Pedalling Index";
            this.PIS.Name = "PIS";
            // 
            // lrds
            // 
            this.lrds.HeaderText = "Left Right balance";
            this.lrds.Name = "lrds";
            // 
            // Calendar_View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 584);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lstData);
            this.Controls.Add(this.CalendarData);
            this.Controls.Add(this.btnOpenCalendar);
            this.Name = "Calendar_View";
            this.Text = "Calendar_View";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton btnOpenCalendar;
        private System.Windows.Forms.MonthCalendar CalendarData;
        private System.Windows.Forms.ListBox lstData;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimeInterval;
        private System.Windows.Forms.DataGridViewTextBoxColumn HeartRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Speeds;
        private System.Windows.Forms.DataGridViewTextBoxColumn cadences;
        private System.Windows.Forms.DataGridViewTextBoxColumn Altitudes;
        private System.Windows.Forms.DataGridViewTextBoxColumn PowerS;
        private System.Windows.Forms.DataGridViewTextBoxColumn powerBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn PIS;
        private System.Windows.Forms.DataGridViewTextBoxColumn lrds;
    }
}