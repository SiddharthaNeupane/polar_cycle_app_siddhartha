﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Polar_Cycle_App_Siddhartha
{
    class main_class
    {
        OpenFileDialog openFileDialog = new OpenFileDialog();

        public string fileDialogue(OpenFileDialog openFileDialog)
        {
            openFileDialog.Title = "Open File";
            openFileDialog.Filter = "HRM files (*.hrm)|*.hrm|All files (*.*)|*.*";
            openFileDialog.FilterIndex = 2;
            openFileDialog.RestoreDirectory = true;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
            }
            StreamReader reader = new StreamReader(openFileDialog.FileName, System.Text.Encoding.Default);
            string read_hrm_text = reader.ReadToEnd();
            return read_hrm_text;
        }
    }
}
