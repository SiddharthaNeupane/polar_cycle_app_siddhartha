﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Polar_Cycle_App_Siddhartha
{
    public partial class IntervDetection : MetroFramework.Forms.MetroForm
    {
        public IntervDetection()
        {
            InitializeComponent();
        }

        private void IntervDetection_Load(object sender, EventArgs e)
        {
            int v = 0;

            try
            {
                lblThres.Text = OpenFile.threholdValueGlobal.ToString();

                //MessageBox.Show(mainDashboard.intervalDetectionData.Count().ToString()+" "+ mainDashboard.powerData.Count.ToString());

                for (int i = 0; i < OpenFile.intervalDetectionData.Count(); i++)
                {
                    this.grd_interval.ClearSelection();
                    this.grd_interval.Rows.Add();
                    this.grd_interval.Rows[i].Cells[0].Value = "Interval " + OpenFile.intervalDetectionData[i];

                    this.grd_interval.Rows[i].Cells[1].Value = OpenFile.powerInterval[i];

                    // lst_interval.Items.Clear();

                }

                for (int j = 1; j <= OpenFile.intervalDetectionData.Last(); j++)
                {
                    listBox1.Items.Add("Interval " + j);
                }

                detect();
               

            }
            catch (Exception ex)
            {
                MessageBox.Show("Some error Occurred: \n" + ex);
            }
         }

        private void listBox1_MouseClick(object sender, MouseEventArgs e)
        {
            
        }
        
        public void detect()
        {
            List<double> powerAv = new List<double>();

            foreach (DataGridViewRow row in grd_interval.Rows)
            {
                // if (row.Cells[0].Value.ToString() == listBox1.SelectedItem.ToString())
                // {
                powerAv.Add(Convert.ToDouble(row.Cells[1].Value));
                double intervalAvg = powerAv.Average();
                lblAvg.Text = intervalAvg.ToString();
                //}


            }
        }
    }
}
